import Ember from 'ember';

export default Ember.Service.extend({
	email: "",
	userId: "",
	admin: "",
	companyId: "",
	setProperty(key, value) {
		this.set(key, value);
	},
	getProperty(key) {
		return (this.get(key));
	}
});

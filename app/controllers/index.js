import Ember from 'ember';

export default Ember.Controller.extend({
	filterValue: undefined,

	properties: Ember.computed(function() {
		return this.store.peekAll('property');
	}),

	filteredProperties: Ember.computed('filterValue', function() {
		var self = this;

		if (this.get('filterValue')) {

			var props = this.get('properties').filter(function(property) {
				var filterVal = _.lowerCase(self.get('filterValue')),
					street = _.lowerCase(property.get('street')),
					street2 = _.lowerCase(property.get('street2')),
					city = _.lowerCase(property.get('city')),
					isInStreet = _.includes(street, filterVal),
					isInStreet2 = _.includes(street2, filterVal),
					isInCity = _.includes(city, filterVal);

				return isInStreet || isInStreet2 || isInCity;
			});

			$(".ui.dropdown").dropdown();

			return props;

		} else {
			$(".ui.dropdown").dropdown();

			return this.get('properties');
		}

	})
});

import Ember from 'ember';
import _ from 'lodash';

(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");

ga("create", "UA-75212249-1", "auto");

export default Ember.Controller.extend({
  qaIndex: 0,
  showName: false,
  currentQA: Ember.computed('qaIndex', function() {
    return this.model[this.get('qaIndex')];
  }),
  actions: {
    nextQuestion(answer) {
      ga("send", "event", "quiz", "question answered", "Question #" + this.get("qaIndex") + ": " + answer);

      if (this.get('qaIndex') === 0) {
        document.inlineAccountCreation = {};

        if (_.includes(answer, "attorney")) {
          document.inlineAccountCreation.job = 4;
        } else if (_.includes(answer, "title")) {
          document.inlineAccountCreation.job = 2;
        } else {
          document.inlineAccountCreation.job = 7;
        }
      }


      var nextQuestion = this.get('qaIndex') + 1;
      if (nextQuestion >= this.model.length) {
        this.set("showName", true);
        setTimeout(function() {
          $(".progress").progress();
        }, 0);
      } else {
        $(".progress").progress({percent: nextQuestion * 16.66});
        this.set('qaIndex', nextQuestion);
      }
    }
  }
});

import Model from 'ember-data/model';
import attr from 'ember-data/attr';
// import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
	user: attr("number"),
	docTypeId: attr("number"),
	name: attr("string"),
	read: attr("string"),
	update: attr("string"),
	active: attr("number"),
	isMine: attr("number"),
	autocreate: attr("autocreate")
});

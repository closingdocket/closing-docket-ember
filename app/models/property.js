import Model from 'ember-data/model';
import attr from 'ember-data/attr';
// import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
	propId: attr("number"),
	price: attr("number"),
	role: attr("string"),
	state: attr("string"),
	street: attr("string"),
	street2: attr("string"),
	zipcode: attr("string"),
	color: attr("string"),
	company: attr("number"),
	userInParty: attr("boolean"),
	canBeModified: attr("boolean"),
	closedate: attr("date"),
	docDrops: attr("array"),
	tasks: attr("array"),
	party: attr("array"),
	city: attr("string"),
	percentage: attr("string"),
	count: attr("string")
});

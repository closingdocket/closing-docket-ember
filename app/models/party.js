import Model from 'ember-data/model';
import attr from 'ember-data/attr';
// import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
	contactCo: attr("number"),
	contact: attr("number"),
	role: attr("number"),
	user: attr("number"),
	firstname: attr("string"),
	lastname: attr("string"),
	email: attr("string"),
	job: attr("number"),
	phone: attr("string"),
	fax: attr("string"),
	nmls: attr("string"),
	license: attr("string"),
	trust: attr("number")
});

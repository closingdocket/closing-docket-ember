import Model from 'ember-data/model';
import attr from 'ember-data/attr';
// import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
	user: attr("number"),
	doctype: attr("number"),
	docname: attr("string"),
	docext: attr("string"),
	name: attr("string"),
	read: attr("string"),
	update: attr("string")
});

// [{"user":2,"doctype":24,"docname":"HUD - Most Calculations",
// "docext":"pdf","name":"1099-s form Test","read":"10,11","update":"4,10,11"}]

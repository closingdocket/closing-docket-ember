import Model from 'ember-data/model';
import attr from 'ember-data/attr';
// import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
	taskId: attr("number"),
	todo: attr("string"),
	user: attr("number"),
	complete: attr("number"),
	lastname: attr("string"),
	firstname: attr("string")
});

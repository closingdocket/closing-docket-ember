import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com",
	sessionToken = document.userCache.session,
	company = document.userCache.company | 0;

export default Ember.Component.extend({
	actions: {
		inviteUser() {
			$("#successful-quick-invite-notification").slideDown().delay(1000).slideUp();
				var email = $("#quick-invite-user-email").val(),
				role = $("#quick-invite-user-role").dropdown("get value")[0] | 0, firstName = $("#quick-invite-user-first-name").val(),
				lastName = $("#quick-invite-user-last-name").val();


			Ember.$.ajax({
				method: "POST",
				url: coreURL + "/contact?session=" + sessionToken,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						email: email,
						job: role,
						firstname: firstName,
						lastname: lastName
					}
				),
				success: function(data) {
				},
				error: function(data) {
					console.warn("Inviting user failed.");
				}
			});

			Ember.$.ajax({
				method: "POST",
				url: coreURL + "/email/invite?session=" + sessionToken,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						email: email
					}
				),
				success: function(data) {
					$("#quick-invite-user-email").val("");
					$("#quick-invite-user-first-name").val("");
					$("#quick-invite-user-last-name").val("");
					$("#quick-invite-user-role").dropdown("set exactly", "empty");
					$("#successful-quick-invite-notification").html(firstName + " " + lastName + " was invited and added to your contact list.<br>");
					$("#parties-box").modal("hide");
				},
				error: function(data) {
					console.warn("Adding party member failed!");
				}
			});
		},
		addParty() {
			$("#successful-quick-add-notification").slideDown().delay(1000).slideUp();
			var role = $("#add-contact-role").dropdown("get value")[0] | 0,
				contact = $("#add-contact-to-party").dropdown("get value")[0] | 0, propId = $("#quick-share-header").data("propId");

			Ember.$.ajax({
				method: "POST",
				url: coreURL + "/party?session=" + sessionToken,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						company: company,
						role: role,
						contact: contact,
						property: propId
					}
				),
				success: function(data) {
				},
				error: function(data) {
					console.warn("Adding contact to party failed.");
				}
			});
		}
	}
});

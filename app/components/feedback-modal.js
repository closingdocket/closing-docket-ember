import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session;

export default Ember.Component.extend({
	actions: {
		sendFeedback() {
			if ($("#feedback-field").val().length > 3) {
				var self = this;
				Ember.$.ajax({
					method: "POST",
					url: coreURL + "/email/comment?session=" + sessionToken,
					dataType: "json",
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(
						{
							"comment": $("#feedback-field").val() || null
						}
					),
					success: function(data) {
						$("#feedback-field").val("");
					},
					fail: function() {
						console.warn("sending feeback had issue!");
					}
				});
			}
		},
	}
});

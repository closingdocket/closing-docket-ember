import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		viewExpandedProperty(property) {
			var street = property.get("street");

			this.sendAction("zoomIntoProperty", street);
		}
	},
	didInsertElement() {
		$('.progress').progress();
	}
});

import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		hideModals() {
			this.sendAction("hideModals");
		}
	}
});

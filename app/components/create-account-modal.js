import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		createAccount() {
			this.sendAction("createAccount");
		},
		hideModals() {
			this.sendAction("hideModals");
		},
		viewTermsAndConditions() {
			$("#terms-and-conditions-box").fadeIn();
		}
	},
	didInsertElement() {
		var firstName = $("#create-account-first-name"), lastName = $("#create-account-last-name"),
			email = $("#create-account-email"), confirmEmail = $("#create-account-email-confirm"),
			password = $("#create-account-password"), confirmPassword = $("#create-account-password-confirm"),
			loginEmail = $("#login-email"), loginPassword = $("#login-password");

		firstName.keyup(function(event) {
			$(this).val(_.upperFirst($(this).val()));

			if ($($(this).parent()[0].previousElementSibling).is(":visible")) {
				handleConditionResult((firstName.val().length > 1), "Name not long enough.", this);
			}

			checkIfEverythingIsValid(event);
		});

		lastName.keyup(function(event) {
			$(this).val(_.upperFirst($(this).val()));

			if ($($(this).parent()[0].previousElementSibling).is(":visible")) {
				handleConditionResult((lastName.val().length > 1), "Name not long enough.", this);
			}

			checkIfEverythingIsValid(event);
		});

		email.keyup(function(event) {
			if ($($(this).parent()[0].previousElementSibling).is(":visible")) {
				handleConditionResult(isValidEmail(email.val()), "Invalid email.", this);
			}

			checkIfEverythingIsValid(event);
		});

		confirmEmail.keyup(function(event) {
			if ($($(this).parent()[0].previousElementSibling).is(":visible")) {
				handleConditionResult([isValidEmail(confirmEmail.val()), email.val() === confirmEmail.val()], ["Invalid email.", "Emails do not match."], this);
			}

			checkIfEverythingIsValid(event);
		});

		password.keyup(function(event) {
			if ($($(this).parent()[0].previousElementSibling).is(":visible")) {
				handleConditionResult((password.val().length > 1), "Password not long enough.", this);
			}

			checkIfEverythingIsValid(event);
		});

		confirmPassword.keyup(function(event) {
			if ($($(this).parent()[0].previousElementSibling).is(":visible")) {
				handleConditionResult([confirmPassword.val().length > 1, password.val() === confirmPassword.val()], ["Invalid password.", "Passwords do not match."], this);
			}

			checkIfEverythingIsValid(event);
		});


		firstName.focusout(function() {
			handleConditionResult((firstName.val().length > 1), "Name not long enough.", this);
			checkIfEverythingIsValid();
		});

		lastName.focusout(function() {
			handleConditionResult((lastName.val().length > 1), "Name not long enough.", this);
			checkIfEverythingIsValid();
		});

		email.focusout(function() {
			handleConditionResult(isValidEmail(email.val()), "Invalid email.", this);
			checkIfEverythingIsValid();
		});

		confirmEmail.focusout(function() {
			handleConditionResult([isValidEmail(confirmEmail.val()), email.val() === confirmEmail.val()], ["Invalid email.", "Emails do not match."], this);
			checkIfEverythingIsValid();
		});

		password.focusout(function() {
			handleConditionResult((password.val().length > 1), "Password not long enough", this);
			checkIfEverythingIsValid();
		});

		confirmPassword.focusout(function() {
			handleConditionResult([confirmPassword.val().length > 1, password.val() === confirmPassword.val()], ["Invalid password.", "Passwords do not match."], this);
			checkIfEverythingIsValid();
		});

		if (window.location.pathname === "/invite/") {
			var args = window.location.search.split("&"),
				argsObj = {}, key, value;

			_.forEach(args, function(arg) {
				key = arg.split("=")[0].replace(/\//gi, "").replace(/\?/gi, "");
				value = arg.split("=")[1];
				argsObj[key] = value;
			});

			if (argsObj.first) {
				$("#create-account-first-name").val(_.upperFirst(_.toLower(argsObj.first)));
				$("#header-message").html(`${_.upperFirst(_.toLower(argsObj.first))}, welcome to Closing Docket!`);
				$("#first-invited").hide();
			}

			if (argsObj.last) {
				$("#create-account-last-name").val(_.upperFirst(_.toLower(argsObj.last)));
				$("#last-invited").hide();
			}

			if (argsObj.email) {
				$("#create-account-email").val(argsObj.email);
				$("#create-account-email-confirm").val(argsObj.email);
			}

			$("#email-invited").hide();
			$("#create-account-box").fadeIn();
			$("#create-account-first-name").focus();
		}
	}
});

function checkIfEnterWasPressed(event, callback) {
	if (event.keyCode === 13) {
		if (callback) {
			callback();
		} else {
			$("#create-account-button").click();
		}
	}
}

function changeAccountButton(status) {
	$("#create-account-button").attr("disabled", (status === "disable" ? status : false));
}

function handleConditionResult(condition, failMessage, context) {
	var currentMessage, status = true, checkValidity = function(cond, message, cont) {
		if (cond) {
			$($(cont).parent()[0].previousElementSibling).slideUp();
			$(cont).parent().removeClass("error");
		} else {
			$($(cont).parent()[0].previousElementSibling).html(message);
			$($(cont).parent()[0].previousElementSibling).slideDown();
			$(cont).parent().addClass("error");
		}
	}
	if (_.isArray(condition)) {
		_.forEach(condition, function(curCond, idx) {
			if (!curCond) {
				status = false;
				currentMessage = failMessage[idx];
			}
		});
		checkValidity(status, currentMessage, context);
	} else {
		checkValidity(condition, failMessage, context);
	}
}

function isValidEmail(email) {
	var valid = false, mustInclude = ["@", "."], includesKeyChars = function() {
		var includesEverything = true;
		_.forEach(mustInclude, function(char) {
			if (!_.includes(email, char)) {
				includesEverything = false;
			};
		});

		return includesEverything;
	};
	if (email.length > 4 && includesKeyChars()) {
		valid = true;
	}

	return valid;
}

function checkIfEverythingIsValid(event) {
	var firstName = $("#create-account-first-name"), lastName = $("#create-account-last-name"),
		email = $("#create-account-email"), confirmEmail = $("#create-account-email-confirm"),
		password = $("#create-account-password"), confirmPassword = $("#create-account-password-confirm");

	if ((firstName.val().length > 1) && (lastName.val().length > 1) && isValidEmail(email.val()) && (isValidEmail(confirmEmail.val()) && email.val() === confirmEmail.val()) && (password.val().length > 1) && (confirmPassword.val().length > 1 && password.val() === confirmPassword.val())) {
	// if (true) {
		event && checkIfEnterWasPressed(event);
		changeAccountButton("enable");
	} else {
		changeAccountButton("disable");
	}
}

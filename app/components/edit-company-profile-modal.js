import Ember from 'ember';

var baseURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session;

export default Ember.Component.extend({
	actions: {
		saveChangeToCompany() {
			var self = this;

			Ember.$.ajax({
				method: "PUT",
				url: baseURL + "/company?session=" + sessionToken,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						"name": $("#edit-company-name").val() || null,
						"street": $("#edit-company-street").val() || null,
						"street2": $("#edit-company-street-2").val() || null,
						"city": $("#edit-company-city").val() || null,
						"state": $("#edit-company-state").val() || null,
						"zipcode": $("#edit-company-zipcode").val() || null,
						"fax": $("#edit-company-fax").val() || null,
						"nmls": $("#edit-company-nmls").val() || null,
						"license": $("#edit-company-license").val() || null
					}
				),
				success: function(data) {
					$("#edit-company-profile-box").modal("hide");
				},
				fail: function() {
					console.warn("save to company!");
				}
			});
		}
	}
});

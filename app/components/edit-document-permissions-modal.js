import Ember from 'ember';

var baseURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session;

export default Ember.Component.extend({
	actions: {
		uncheckAllRead() {
			$("#read-permissions").dropdown("set exactly", []);
		},
		checkAllRead() {
			$("#read-permissions").dropdown("set exactly", ["1","2","3","4","5","6","7","8","9","10","11"]);
		},
		uncheckAllUpdate() {
			$("#update-permissions").dropdown("set exactly", []);
		},
		checkAllUpdate() {
			$("#update-permissions").dropdown("set exactly", ["1","2","3","4","5","6","7","8","9","10","11"]);
		},
		updateDocumentPermissions() {
			var self = this,
				dataObj = {
					name: $("#permissions-drop-name").data("docname"),
					read: $("#read-permissions").val().join(","),
					update: $("#update-permissions").val().join(","),
					autocreate: $("#edit-permissions-autocreate").prop("checked"),
					active: $("#edit-permissions-active").prop("checked"),
					id: $("#doc-drop-permissions-id").data("doctypeid") | 0
				};

			Ember.$.ajax({
				method: "PUT",
				url: "https://app-closingdocket.rhcloud.com/doctype?session=" + document.userCache.session + "&id=" + dataObj.id,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						"active": dataObj.active,
						"autocreate": dataObj.autocreate,
						"name": dataObj.name,
						"read": dataObj.read,
						"update": dataObj.update
					}
				),
				success: function(data) {
				},
				fail: function() {
					console.warn("updating permission type changes failed!");
				}
			});
		}
	},
	didInsertElement() {
		$(".ui.checkbox").checkbox();
	}
});

import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session;

export default Ember.Component.extend({
	actions: {
		restoreProperty(property) {
			var company = property.get("company"), propId = property.get("propId"), self = this;

			Ember.$.ajax({
				method: "PUT",
				url: `${coreURL}/property/archive?session=${sessionToken}&company=${company}&property=${propId}&archive=true`,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify({}),
				success: function(data) {
					self.sendAction("refreshData");
				},
				error: function() {
					console.warn("Restoring property failed.");
				}
			});
		},
		permanetelyDeleteProperty(property) {
			console.log("Permenately deleting property:", property.get("street"));
		}
	}
});

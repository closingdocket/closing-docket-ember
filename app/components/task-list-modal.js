import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session;

export default Ember.Component.extend({
	didInsertElement() {
		$("#new-task-description").keyup(function(event){
			if (event.which === 13 && $(this).val().length > 0) {
				$("#new-task-add-button").click();
			}
		});
	},
	actions: {
		addTask() {
			this.sendAction("addTask");
		},
		refreshData() {
			this.sendAction("refreshData");
		}
	}
});

import Ember from "ember";

function checkIfValid() {
	let advanceToGettingStartedButton = $("#inline-create-account");
	let first = $("#inline-first-name").val();
	let last = $("#inline-last-name").val();
	let job = $("#inline-job").val();

	if (first.length > 2 && last.length > 2 && job) {
		advanceToGettingStartedButton.removeClass("disabled");
	} else {
		advanceToGettingStartedButton.addClass("disabled");
	}
}

export default Ember.Component.extend({
	actions: {
		advanceToGettingStarted() {
			let first = $("#inline-first-name").val();
			let last = $("#inline-last-name").val();
			let job = $("#inline-job").val();

			document.inlineAccountCreation = {first: first, last: last, job: job};
			this.sendAction("goToGettingStarted");
		}
	},
	didInsertElement() {
		$("#inline-first-name").change(function() {
			checkIfValid();
		});

		$("#inline-last-name").change(function() {
			checkIfValid();
		});

		$("#inline-job").change(function() {
			checkIfValid();
		});
	}
});

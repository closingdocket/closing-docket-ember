import Ember from 'ember';

var sessionToken = document.userCache.session, coreURL = "https://app-closingdocket.rhcloud.com",
	company = document.userCache.company | 0;

export default Ember.Component.extend({
	actions: {
		viewPermissions(docDrop) {
			// Start loader
			$("#index-loader").fadeIn();

			var permissionArr;

			ga('send', 'event', "permissions", "view", docDrop.name);

			_.forEach(["read", "update"], function(permissionType) {
				permissionArr = docDrop[permissionType].split(",");
				$("#" + permissionType + "-permissions").dropdown("set selected", permissionArr);
				$("#permissions-drop-name").html("<span id='doc-drop-permissions-id' data-docTypeId='" + docDrop.doctype + "'>Edit the permissions for <strong>" + docDrop.name + "</span></strong>");
				$("#permissions-drop-name").data("docname", docDrop.name);
			});

			$("#edit-document-permissions-box").modal({ autofocus: false}).modal("show");

			// End loader
			$("#index-loader").fadeOut();
		},
		archiveProperty() {
			this.sendAction("archiveProperty");
		},
		addParty(contactList) {
			// start loader
			$("#index-loader").fadeIn();

			$("#no-party-members-warning").slideUp();

			var companyId = $("#manage-parties-header").data("companyid"),
				propId = $("#manage-parties-header").data("propid"),
				contact = ($("#add-new-party-contact-list").dropdown("get value")[0] | 0),
				role = ($("#add-new-party-role").dropdown("get value")[0] | 0), self = this;

			ga('send', 'event', "party member", "add", "contactId: " + contact);

			Ember.$.ajax({
				method: "POST",
				url: `${coreURL}/party?session=${sessionToken}`,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						contact: contact,
						role: role,
						property: propId,
						company: companyId
					}
				),
				success: function() {
					getPartyList(propId, companyId, contactList, function() {});
				},
				error: function(data) {
					// console.warn("Adding a party member errored.");
				}
			});
		},
		viewPropertyInfo(property) {
			var street = property.get("street"), street2 = property.get("street2"),
				city = property.get("city"), state = property.get("state"), zipcode = property.get("zipcode"),
				price = property.get("price"), closedate = property.get("closedate");

			ga('send', 'event', "property info", "view", "property: " + street);

			$("#edit-property-street").val(street);
			$("#edit-property-street2").val(street2);
			$("#edit-property-city").val(city);
			$("#edit-property-state").val(state);
			$("#edit-property-zipcode").val(zipcode);
			$("#edit-property-price").val(price);

			$('#edit-property-closedate').datepicker();
			$('#edit-property-closedate').datepicker( "option", "dateFormat", "M d, yy");
			$("#edit-property-closedate").val(moment(closedate).format("MMM DD, YYYY"));

			$("#property-info-header").html("Edit Information about " + street);

			$("#property-info-header").data("property", property.get("propId"));
			$("#property-info-header").data("company", property.get("company"));


			$("#edit-property-info-box").modal("setting", "transition", "vertical flip");
			$("#edit-property-info-box").modal("show");
		},
		saveChangesToProperty() {
			var data, self = this, coreURL = "https://app-closingdocket.rhcloud.com",
				property = $("#property-info-header").data("property"), company = $("#property-info-header").data("company");

			ga('send', 'event', "property info", "edit", "property: " + $("#edit-property-street").val());

			Ember.$.ajax({
				method: "PUT",
				url: coreURL + "/property?session=" + sessionToken + "&company=" + company + "&property=" + property,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						street: $("#edit-property-street").val() || null,
						street2: $("#edit-property-street2").val() || null,
						city: $("#edit-property-city").val() || null,
						state: $("#edit-property-state").val() || null,
						zipcode: $("#edit-property-zipcode").val() || null,
						price: $("#edit-property-price").val() || null,
						closedate: moment($("#edit-property-closedate").val()).format("YYYY-MM-DD")
					}
				),
				success: function(data) {
					self.sendAction("refreshData");
				},
				error: function() {
					// console.warn("Update property info errored.");
				}
			});
		},
		saveChangesToParty() {
			var coreURL = "https://app-closingdocket.rhcloud.com",
				partyList = $("#manage-party-list").children(),
				property = $("#manage-parties-header").data("propid") | 0,
				companyId =  $("#manage-parties-header").data("companyid") | 0,
				currentContact, contact, currentRole, role, trust, user,
				self = this;

			ga('send', 'event', "party", "edit");

			_.forEach(partyList, function(el){
			 	contact = $(el).find(".manage-party-contact-list").dropdown("get value") | 0;
			 	role = $(el).find(".manage-party-user-role-dropdown").dropdown("get value") | 0;
			 	currentRole = $(el).data("role");
			 	currentContact = $(el).data("contact");
			 	user = $($($(el).find(".manage-party-contact-list")).children()[0]).data("userid");
			 	trust = $(el).find(".manage-party-trust-dropdown").dropdown("get value") | 0;

				if (contact > 0 && role > 0) {


	  			(function(user, role, trust, contact) {
	  				Ember.$.ajax({
	  				method: "PUT",
	  				url: `${coreURL}/party?session=${sessionToken}&company=${companyId}&property=${property}&contact=${currentContact}&role=${currentRole}`,
	  				dataType: "json",
	  				contentType: "application/json; charset=utf-8",
	  				data: JSON.stringify(
	  					{
	  						contact: contact,
	  						role: role
	  					}
	  				),
	  				success: function(data) {
	  				},
	  				error: function(data) {
	  					// console.warn("Updating party errored.");
	  				}
  				}).then(function() {
		  			Ember.$.ajax({
		  				method: "PUT",
		  				url: `${coreURL}/user/trust?session=${sessionToken}`,
		  				dataType: "json",
		  				contentType: "application/json; charset=utf-8",
		  				data: JSON.stringify(
		  					{
		  						other: user,
		  						role: role,
		  						level: trust
		  					}
		  				),
		  				success: function(data) {
	  						// self.sendAction("refreshData");
		  				},
		  				error: function(data) {
		  					// console.warn("Updating party trust errored.");
		  				}
		  			});
  				})

	  		}(user, role, trust, contact));

  			}
			});
		},
		addTask() {
			this.sendAction("addTask");
		},
		refreshData() {
			this.sendAction("refreshData");
		},
		viewContacts(contacts) {
			this.sendAction("viewContacts", contacts);
			// action handled in index.js
		},
		confirmArchiveProperty(property) {
			ga('send', 'event', "property", "archive", "confirmation");

			$("#archive-property-header").html("Archive " + property.get("street"));
			$("#archive-property-button").data("company", property.get("company"));
			$("#archive-property-button").data("property", property.get("propId"));

			$("#archive-property-box").modal().modal("show");
		},
		viewQuickShare(property, docDrop) {
			ga('send', 'event', "document", "share", "document name: " + docDrop.docname);

			$("#quick-share-header").html("Share " + docDrop.docname);
			$("#quick-share-header").data("propId", property.get("propId"));
			$("#quick-share-box").modal("setting", "transition", "vertical flip");
			$("#quick-share-box").modal("show");
		},
		viewTasks(property) {
			var taskList = [], companyId = property.get("company"),
				propertyId = property.get("propId"), name, found,
				numOfUserTasks = 0;

			ga('send', 'event', "tasks", "view", "property: " + propertyId);

			_.forEach(property.get("tasks"), function(task) {
				name = task.get("firstname") + " " + task.get("lastname");
				found = false;

				_.forEach(taskList, function(taskInList) {
					if (taskInList.name === name && !found) {
						taskInList.list.push(
							{
								description: task.get("todo"),
								id: task.get("id"),
								user: task.get("user"),
								completed: task.get("complete")
							}
						);
						found = true;
					}
				});

				if (!found) {
					if ((task.get("user") | 0) === (document.userCache.user | 0)) {
						numOfUserTasks += 1;
					}

					taskList.push({
						name: name,
						userId: task.get("user"),
						list: [
							{
								description: task.get("todo"),
								id: task.get("id"),
								user: task.get("user"),
								completed: task.get("complete")
							}
						]
					});
				}
			});

			var userTasksTemplate = _.template(
				'<div class="item">' +
					'<div class="header"><img class="ui avatar image" src="/assets/images/default.png"><%= name %></div>' +
					'<div class="content">' +
						'<div class="list <%= isUser %>" id="task-list-items">' +
							'<%= tasks %>' +
						'</div>' +
					'</div>' +
				'</div>'
			),
			taskTemplate = _.template(
				'<div class="item">' +
					'<div class="ui checkbox">' +
						'<input id="<%= id %>" type="checkbox" <%= checked %> tabindex="0" class="hidden">' +
						'<label class="list-item"><%= description %></label>' +
					'</div>' +
				'</div>'
			), tasks;

			$("#task-list-list").html("");

			var checkboxes = [];

			_.forEach(taskList, function(user) {
				tasks = "";
				_.forEach(user.list, function(task) {
					tasks += taskTemplate({
						description: task.description,
						checked: task.completed ? "checked" : "",
						id: "task-checkbox-" + propertyId + "-" + task.user + "-" + task.id
					});

					checkboxes.push(
						{
							id: "task-checkbox-" + propertyId + "-" + task.user + "-" + task.id,
							propId: propertyId,
							userId: task.user,
							compId: companyId,
							checkboxId: task.id
						}
					);

				});

				$("#task-list-list").append(
					userTasksTemplate({
						isUser: user.userId === (document.userCache.user | 0) ? "user-owned" : "non-user-owned",
						name: user.name,
						tasks: tasks
					})
				);
			});

			_.forEach(checkboxes, function(checkbox) {
				$("#" + checkbox.id).change(function() {
					Ember.$.ajax({
						method: "PUT",
						url: `${coreURL}/task?session=${sessionToken}&company=${checkbox.compId}&property=${checkbox.propId}&id=${checkbox.checkboxId}`,
						dataType: "json",
						contentType: "application/json; charset=utf-8",
						data: JSON.stringify(
							{
								complete: $(this).prop("checked")
							}
						),
						success: function(data) {
						},
						error: function() {
							// console.warn("Modifying status of task errored.");
						}
					});
				});
			});

			if (!numOfUserTasks) {
				$("#task-list-list").prepend(
					userTasksTemplate({
						isUser: "user-owned",
						name: "You",
						tasks: ""
					})
				);
			}

			$("#new-task-description").data("property", propertyId);
			$("#new-task-description").data("company", companyId);

			$("#task-list-header").html("Manage Tasks for " + property.get("street"));
			$(".ui.checkbox").checkbox();
			$("#task-list-box").modal("setting", "transition", "vertical flip");
			$("#task-list-box").modal("setting", "closable", false);
			$("#task-list-box").modal("show");
		},
		addDrop(property) {
			var docTypeId = $("#new-drop-" + property.get("company") + "-" + property.get("propId")).dropdown("get value")[0] | 0;
			this.sendAction("addDrop", property, docTypeId);
			$(".ui.dropdown").dropdown(
				{
					onNoResults(searchValue) {
						// console.log("search value:", searchValue);
					},
					onChange(value, text, choice) {
						// console.log("value:", value, "text:", text, "choice:", choice);
					},
					allowCategorySelection: true
				}
			);
		},
		deleteDocument(property, docDrop) {
			this.sendAction("deleteDocument", property, docDrop);
		},
		deleteDrop(property, docDrop) {
			this.sendAction("deleteDrop", property, docDrop);
		},
		viewParties(property, contactList) {
			// console.log(`userInParty before: ${property.get("userInParty")}`);
			property.set("userInParty", true);
			// console.log(`userInParty after: ${property.get("userInParty")}`);
			var companyId = property.get("company");

			$("#manage-parties-header").html("Manage Parties for " + property.get("street"));

			ga('send', 'event', "party", "view", "property: " + property.get("street"));

			getPartyList(property.get("propId"), companyId, contactList, function() {
				$("#parties-box").modal("setting", "allowMultiple", true);
				$("#parties-box").modal("setting", "transition", "vertical flip");
				$("#parties-box").modal("show");
			});
		}
	},
	didInsertElement() {
		$(function () {
			setTimeout(function() {
				attachEvents();
			}, 500);
		});
	}
});

function attachEvents() {
	var data, coreURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session
		self = this;

	$('.fa-5x').popup();
	$('.fa-4x').popup();
	$(".title-popup").popup();

	_.forEach($(".add-current-user-dropdown"), function(dropdown) {
		$(dropdown).change(function() {
			$($(dropdown).parent()).siblings().removeClass("disabled");
		});
	});

	_.forEach($(".invisible-input.street"), function(el) {
		$(el).change(function() {
			data = $($(el).parent().siblings()[0]).text().split("|");
			data = {
				propertyId: data[0],
				companyId: data[1]
			};

			Ember.$.ajax({
				method: "PUT",
				url: coreURL + "/property?session=" + sessionToken + "&company=" + data.companyId + "&property=" + data.propertyId,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						street: $(el).val().length ? $(el).val() : null
					}
				),
				success: function(data) {
				},
				error: function() {
					// console.warn("Updating the street name errored.");
				}
			});
		});
	});

	_.forEach($(".close-date"), function(el) {
		$(el).datepicker();

		$(el).change(function() {
		$(el).datepicker( "option", "dateFormat", "M d, yy");
			data = {
				propertyId: $(this).prev().data("property"),
				companyId: $(this).prev().data("company")
			};

			Ember.$.ajax({
				method: "PUT",
				url: coreURL + "/property?session=" + sessionToken + "&company=" + data.companyId + "&property=" + data.propertyId,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						closedate: moment($(el).val()).format("YYYY-MM-DD")
					}
				),
				success: function(data) {
				},
				error: function() {
					// console.warn("property archive errored!");
				}
			});
		});
	});

	$("#property-filter").keyup(function() {
		attachEvents();
	});

	$('.progress').progress();

	_.forEach($(".add-new-drop-dropdown"), function(dropdown) {

		var dropdownContext = dropdown;

		$(dropdown).dropdown({
			onChange(value, text, choice) {

				dropdownContext = $(dropdownContext.firstChild);

				var companyId = $(dropdownContext).data('company');
				var propId = $(dropdownContext).data('propid');
				var id = `#add-doc-${companyId}-${propId}`;
				var addButton = $(id);

				if (value !== "empty") {
					addButton.slideDown();
				} else {
					addButton.slideUp();
				}
			}
			// onNoResults(searchValue) {
				// $(dropdown).keyup((key) => {
				// 	console.log("key:", key.which);
				// 	if (key.which === 13) {
				// 		$(dropdown).off("keyup"); // remove the event handler
				// 		$("#create-document-type-box").modal("show");
				// 		console.log("creating new document:", searchValue);
				// 	}

				// });
			// },
			// allowAdditions: true,
			// onAdd(addedValue, addedText, addedChoice) {
			// 	console.log("addedValue:", addedValue, "addedText:", addedText, "addedChoice:", addedChoice);
			// },
			// onLabelCreate(value, text) {
			// 	console.log("value:", value, "Text:", text);
			// }
		});
	});

	_.forEach($('.file-upload'), function(upload) {
		upload = $(upload);

		var doctypeId = upload.data("doctype"),
			propertyId = upload.data("property"),
			companyId = upload.data("company"),
			userId = upload.data("user"),
			uploadId = "#" + companyId + "-" + propertyId + "-" + doctypeId;

	  	(function(doctypeId, propertyId, companyId, userId, uploadId) {
  	  	$(uploadId).fileupload({
			dataType: 'json',
			type: "PUT",
			accept: "*/*",
			url: `${coreURL}/docdrop/doc?session=${sessionToken}&company=${companyId}&property=${propertyId}&user=${userId}&doctype=${doctypeId}`,
			dropZone: $(uploadId),
			done: function(e, data) {
  	      		var uploadEl = $(this), url = coreURL + "/docdrop/doc/" + data.files[0].name + "?session=" + sessionToken + "&company=" + companyId + "&property=" + propertyId + "&user=" + userId + "&doctype=" + doctypeId;

  	      		// succcess checkbox
				$(uploadEl.parent().siblings()[0]).slideDown();

				setTimeout(function() {
					// succcess checkbox
					$(uploadEl.parent().siblings()[0]).slideUp();

					// progress bar
					$(uploadEl.parent().siblings()[1]).slideUp();


					// url wrapper
					$(uploadEl.parent().siblings()[2]).attr("href", url);

					// bottom buttons
					$(uploadEl.parent().siblings()[2]).children().slideDown()
					$(uploadEl.siblings()[3]).slideDown();
				}, 500);
  	     	},
  	    	progressall: function (e, data) {
  	      		var uploadEl = $(this), progress = parseInt(data.loaded / data.total * 100, 10);

  	      		// upload box
				uploadEl.parent().slideUp();

				// progress bar
				$(uploadEl.parent().siblings()[1]).children().css("width", progress + '%');
			}
  	  });
	  }(doctypeId, propertyId, companyId, userId, uploadId));
	});

	$(document).bind('drop dragover', function (e) {
	    e.preventDefault();
	});
}

function getPartyList(propId, companyId, contactList, afterLoad) {
	// start loader
	$("#index-loader").fadeIn();

	var counter = 0,
		partyMemberTemplate = _.template(
		'<div data-contact="<%= contact %>" data-role="<%= role %>" class="ui fluid action input manage-party-list-item" style="margin-bottom: 15px;">' +
			'<%= contactList %>' +
			'<select class="ui selection dropdown manage-party-user-role-dropdown">' +
			'<option value="">Select a Role</option>' +
				'<option <%= lender %> value="1">Lender</option>' +
				'<option <%= mortgage %> value="2">Mortgage Broker</option>' +
				'<option <%= buyerAgent %> value="3">Buyer Agent</option>' +
				'<option <%= listingAgent %> value="4">Listing Agent</option>' +
				'<option <%= title %> value="5">Title Agent</option>' +
				'<option <%= buyer %> value="6">Buyer</option>' +
				'<option <%= seller %> value="7">Seller</option>' +
				'<option <%= regulator %> value="8">Regulator</option>' +
				'<option <%= service %> value="9">Service Provider</option>' +
				'<option <%= settlementAgent %> value="10">Settlement Agent</option>' +
				'<option <%= associate %> value="11">Associate</option>' +
			'</select>' +
			'<select class="ui selection icon dropdown manage-party-trust-dropdown" data-property="<%= property %>" data-contact="<%= contact %>" data-role="<%= role %>">' +
				'<option <%= trustLevel0 %> value="0">Nothing</option>' +
				'<option <%= trustLevel1 %> value="1">Documents</option>' +
				'<option <%= trustLevel2 %> value="2">Documents & Tasks</span></option>' +
			'</select>' +
			'<div type="submit" data-property="<%= property %>" data-contact="<%= contact %>" data-role="<%= role %>" class="ui cancel button remove-party-member">Remove</div>' +
		'</div>'
	), getContactList = function(contactList, selectedId, id) {
		var startSelect = '<select class="ui selection fluid dropdown ' + id + '" data-userid="' + selectedId + '">',
			options = '',
			endSelect = '</select>',
			name, fullName, addMe;

		_.forEach(contactList, function(contact) {
			console.log("contact:", contact);
			// if (!!contact.get("user")) {
				fullName = contact.get("firstname") + " " + contact.get("lastname");
				addMe = ((contact.get("user") === (document.userCache.user | 0)) ? " (Me)" : "");
				name = fullName + addMe;

				options += '<option value="' + contact.get("contactId") + '" ' + (contact.get("contactId") === selectedId ? 'selected=""' : '') + '>' + name + '</option>';
			// }
		});

		return startSelect + options + endSelect;
	};

	contactList = contactList.filter(function(){return true;});

	$("#add-new-party-role").dropdown("set exactly", []);
	$("#add-new-party-contact-list").dropdown("set exactly", []);
	$("#manage-parties-header").data("propid", propId);
	$("#manage-parties-header").data("companyid", companyId);
	$("#manage-party-list").html("");


	Ember.$.getJSON(`${coreURL}/party/list?session=${sessionToken}&company=${companyId}&property=${propId}`).then(function (party) {
		_.forEach(party, function(partyMember, idx) {
			counter = (idx + 1);

			$("#manage-party-list").append(
				partyMemberTemplate({
					contactList: getContactList(contactList, partyMember.contact, "manage-party-contact-list"),
					contact: partyMember.contact,
					role: partyMember.role,
					property: propId,
					lender: partyMember.role === 1 ? "selected=''" : "",
					mortgage: partyMember.role === 2 ? "selected=''" : "",
					buyerAgent: partyMember.role === 3 ? "selected=''" : "",
					listingAgent: partyMember.role === 4 ? "selected=''" : "",
					title: partyMember.role === 5 ? "selected=''" : "",
					buyer: partyMember.role === 6 ? "selected=''" : "",
					seller: partyMember.role === 7 ? "selected=''" : "",
					regulator: partyMember.role === 8 ? "selected=''" : "",
					service: partyMember.role === 9 ? "selected=''" : "",
					settlementAgent: partyMember.role === 10 ? "selected=''" : "",
					associate: partyMember.role === 11 ? "selected=''" : "",
					trustLevel0: partyMember.trust === 0 ? "selected=''" : "",
					trustLevel1: partyMember.trust === 1 ? "selected=''" : "",
					trustLevel2: partyMember.trust === 2 ? "selected=''" : ""
				})
			);
		})

		$("#manage-party-list").append(
			"<div style='width: 100%; text-align: center; margin-bottom: 20px; display: none;' id='no-party-members-warning'>" +
				"<i class='fa fa-exclamation fa-3x' aria-hidden='true'></i><br>" +
				"<span style='font-size: 25px;'>Looks like you don't have anybody assigned to this closing!</span><br/><br/>" +
				"Assign yourself to the role that best fits your job description in this closing.<br>" +
				"Then, either add someone from your contact list or type in an email address to invite someone to this closing." +
			"</div>"
		);

		if (!counter) {
			$("#save-changes-to-party-button").addClass("disabled");
			$("#no-party-members-warning").slideDown();
			$("#property-has-party-" + propId).slideUp();
			$("#property-has-no-party-" + propId).slideDown()
		} else {
			$("#save-changes-to-party-button").removeClass("disabled");
			$("#manage-party-list-label").slideDown();
			$("#property-has-party-" + propId).slideDown();
			$("#property-has-no-party-" + propId).slideUp()

			$("#manage-party-list").prepend(
				`<div class="ui grid" id="manage-party-list-label">
					<div class="one wide column"></div>
					<div class="five wide column">
						<h3><center>Party Member</center></h3>
					</div>
					<div class="three wide column">
						<h3><center>Role</center></h3>
					</div>
					<div class="four wide column">
						<h3><center>Can Access</center></h3>
					</div>
					 <class="three wide column"></div>
				</div>`
			);
		}

		$(".ui.dropdown").dropdown(
			{
				onNoResults(searchValue) {
					// console.log("search value:", searchValue);
				},
				onChange(value, text, choice) {
					// console.log("value:", value, "text:", text, "choice:", choice);
				},
				allowCategorySelection: true
			}
		);

		_.forEach($(".remove-party-member"), function(partyMember) {
			$(partyMember).click(function() {
				$(this).parent().addClass("hidden-party");
				$(this).parent().slideUp();

				var numOfPartyMembers = (($("#manage-party-list").children().length - 2) - $(".hidden-party").length), propId = $(this).data("property"), contact = $(this).data("contact"), role = $(this).data("role");

				if (numOfPartyMembers === 0) {
					$("#manage-party-list-label").slideUp();
					$("#no-party-members-warning").slideDown();
					$("#save-changes-to-party-button").addClass("disabled");
					$("#property-has-party-" + propId).slideUp();
					$("#property-has-no-party-" + propId).slideDown()
				}

				Ember.$.ajax({
					method: "DELETE",
					url: `${coreURL}/party?session=${sessionToken}&company=${company}&property=${propId}&contact=${contact}&role=${role}`,
					dataType: "json",
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify({}),
					success: function(data) {
					},
					error: function() {
						// console.warn("Removing party member errored.");
					}
				});
			});
		});

		// end loader
		$("#index-loader").fadeOut();
		afterLoad();
	});
}

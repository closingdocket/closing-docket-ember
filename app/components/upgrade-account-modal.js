import Ember from 'ember';

const coreURL = "https://app-closingdocket.rhcloud.com";

export default Ember.Component.extend({
    actions: {
        processCard() {
            let expmth = $("#expmth").val() | 0;
            let expyr = $("#expyr").val() | 0;
            let number = $("#number").val();
            let cvc = $("#cvc").val();
            let firstname = $("#firstname").val();
            let lastname = $("#lastname").val();
            let street = $("#street").val();
            let street2 = $("#street2").val();
            let city = $("#city").val();
            let state = $("#state").val();
            let zipcode = $("#zipcode").val();
            let sessionToken = document.userCache.session;

            Ember.$.ajax({
                method: "PUT",
                url: `${coreURL}/pay/card?session=${sessionToken}`,
                dataType: "json",
                data: JSON.stringify({
                    expmth: expmth,
                    expyr: expyr,
                    number: number,
                    cvc: cvc,
                    firstname: firstname,
                    lastname: lastname,
                    street: street,
                    street2: street2,
                    city: city,
                    state: state,
                    zipcode: zipcode
                }),
                contentType: "application/json; charset=utf-8",
                success: function(response) {
                    console.log("card success response:", response);

                    Ember.$.ajax({
                        method: "PUT",
                        url: `${coreURL}/pay/sub?session=${sessionToken}`,
                        data: JSON.stringify({
                            plan: "ent"
                        }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(response) {
                            console.log("plan success response:", response);
                        },
                        error: function(response) {
                            console.log("plan error response:", response);
                        }
                    });
                },
                error: function(response) {
                    console.log("card error response:", response);
                }
            });


            console.log(`
                expmth: ${expmth}
                expyr: ${expyr}
                number: ${number}
                cvc: ${cvc}
                firstname: ${firstname}
                lastname: ${lastname}
                street: ${street}
                street2: ${street2}
                city: ${city}
                state: ${state}
                zipcode: ${zipcode}
            `);
        }
    }
});

import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com";

export default Ember.Component.extend({
	actions: {
		showLoginModal() {
			$("#change-password-box").fadeOut();
			this.sendAction("showLoginModal");
		},
		resetPassword(sessionToken) {
			var password = $("#change-password-password").val(),
				password2 = $("#change-password-confirm-password").val();

			$("#change-password-loader").fadeIn();

			Ember.$.ajax({
				method: "PUT",
				url: `${coreURL}/user/chgpwd?ident=${sessionToken}`,
				dataType: "json",
				data: JSON.stringify(
					{
						password: password,
						password2: password2
					}
				),
				contentType: "application/json; charset=utf-8",
				success: function() {
					$("#change-password-loader").fadeOut();
					$("#change-password-success").slideDown();
					$("#change-password-content").slideUp();
					$("#change-password-login-button").slideDown();
					$("#change-password-button").slideUp();
				},
				error: function() {
					$("#change-password-loader").fadeOut();
					$("#change-password-error-message").slideDown();
				}
			});
		},
		hideModals()  {
			this.sendAction("hideModals");
		}
	},
	didInsertElement() {
		$("#change-password-box").fadeIn();

		$("#change-password-password").keyup(function(key) {
			$("#change-password-error-message").slideUp();
			if ($("#change-password-password").val() === $("#change-password-confirm-password").val()) {
				$("#change-password-button").attr("disabled", false);
				if (key.which === 13) {
					$("#change-password-button").click();
				}
			} else {
				$("#change-password-button").attr("disabled", "disable");
			}
		});

		$("#change-password-confirm-password").keyup(function(key) {
			$("#change-password-error-message").slideUp();
			if ($("#change-password-password").val() === $("#change-password-confirm-password").val()) {
				$("#change-password-button").attr("disabled", false);
				if (key.which === 13) {
					$("#change-password-button").click();
				}
			} else {
				$("#change-password-button").attr("disabled", "disable");
			}
		});
	}
});

import Ember from 'ember';

var sessionToken = document.userCache.session, coreURL = "https://app-closingdocket.rhcloud.com",
	company = document.userCache.company | 0;

export default Ember.Component.extend({
	actions: {
		addCurrentUserToParty(property) {
			var property = property.get("propId"), role = ($($(".add-current-user-to-party-dropdown-" + property)[0]).dropdown("get value") | 0),
				contact = 1, self = this;

			Ember.$.ajax({
				method: "POST",
				url: `${coreURL}/party?session=${sessionToken}`,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						contact: contact,
						role: role,
						property: property,
						company: company
					}
				),
				success: function(data) {
					self.sendAction("refreshData");
				},
				error: function(data) {
					console.warn("Adding yourself to this property's party failed.");
				}
			});
		},
	}
});

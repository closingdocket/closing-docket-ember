import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		viewContacts(contacts) {
			this.sendAction("viewContacts", contacts);
		},
		removeUserFromParty() {
		},
		saveChangesToParty() {
			this.sendAction("saveChangesToParty");
			// this.sendAction("refreshData");
		},
		addParty(contactList) {
			this.sendAction("addParty", contactList);
			$("#add-new-party-role").dropdown("set exactly", []);
			$("#add-new-party-contact-list").dropdown("set exactly", []);
		}
	},
	didInsertElement() {
		var checkIfValid = function() {
			if (($("#add-new-party-contact-list").dropdown("get value")[0] | 0) !== 0 && ($("#add-new-party-role").dropdown("get value")[0] | 0) !== 0) {
				$("#add-party-button").removeClass("disabled");
			}
		};
		$("#add-new-party-role").change(function() {
			checkIfValid();
		});

		$("#add-new-party-contact-list").change(function() {
			checkIfValid();
		});
	}
});

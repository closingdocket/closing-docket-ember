import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		addPropertyButtonClick() {
			this.sendAction("onAddPropertyClick", $('#add-property-name').val());
			$('#add-property-name').val("");
		}
	},
	didInsertElement() {
		$("#add-property-name").keydown(function(event) {
			if (event.which === 13) {
				$("#add-property-button").click();
			}
		});
	}
});

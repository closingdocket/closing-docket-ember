import Ember from "ember";

function checkIfFormsAreValid() {
	let email = $("#getting-started-email").val();
	let password = $("#getting-started-password").val();

	if (email.length > 5 && password.length > 3) {
		$("#get-started-button").removeClass("disabled");
	} else {
		$("#get-started-button").addClass("disabled");
	}
}

function createAccount() {
	ga("send", "event", "account", "created (inline)", "email: " + $("#getting-started-email").val());

	Ember.$.ajax({
		method: "POST",
		url: "https://app-closingdocket.rhcloud.com/user",
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(
			{
				"firstname": document.inlineAccountCreation.first,
				"lastname": document.inlineAccountCreation.last,
				"email": $("#getting-started-email").val(),
				// TO DO implement phone number on account creation
				// phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
				"job": (document.inlineAccountCreation.job | 0) || 7,
				"password": $("#getting-started-password").val(),
				"password2": $("#getting-started-password").val()
			}
		),
		success: function() {
		},
		error: function() {
			// console.warn("account creation failed");
		}
	});
}

export default Ember.Component.extend({
	didInsertElement() {
		$("#getting-started-password").keyup(function() {
			checkIfFormsAreValid();
		});

		$("#getting-started-email").keyup(function() {
			checkIfFormsAreValid();
		});

		$("#get-started-button").click(function() {
			createAccount();
			$("#finish-account-creation-panel").hide("slide", {direction:"left"}, 200);
			$("#confirm-email").html($("#getting-started-email").val());
			setTimeout(function() {
				$("#finish-account-creation-panel").removeClass("six wide column");
				$("#confirm-email-panel").show("slide",{direction:"left"}, 200);
				$("#confirm-email-panel").addClass("six wide column");
			}, 200);
		});
	}
});

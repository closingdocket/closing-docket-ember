import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session;

export default Ember.Component.extend({
	actions: {
		saveChangeToProfile() {
			var self = this,
				data = {
					"email": $("#edit-user-email").val() || null,
					"firstname": $("#edit-user-firstname").val() || null,
					"lastname": $("#edit-user-lastname").val() || null,
					"job": $("#edit-user-job").val() || 4,
					"license": $("#edit-user-license").val() || null,
					"nmls": $("#edit-user-nmls").val() || null,
					"phone": $("#edit-user-phone").val() || null,
					"phone": $("#edit-user-phone").val() || null,
					"phone": $("#edit-user-phone").val() || null,
				};

			// if they entered a password - use it to change their existing one!
			if ($("#edit-user-password").val().length > 1 && $("#edit-user-password-confirm").val().length > 1 && $("#edit-user-password").val() === $("#edit-user-password-confirm").val()) {
				data.password = $("#edit-user-password").val();
				data.password2 = $("#edit-user-password-confirm").val();
			}

			Ember.$.ajax({
				method: "PUT",
				url: coreURL + "/user?session=" + sessionToken,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(data),
				success: function(data) {
					$("#edit-profile-box").modal("hide");
				},
				fail: function() {
					console.warn("save to profile failed!");
				}
			});
		}
	}
});

import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		saveChangesToProperty() {
			this.sendAction("saveChangesToProperty");
		}
	}
});

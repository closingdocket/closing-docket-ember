import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		login() {
			this.sendAction("login");
		},
		hideModals() {
			$("#create-account-box").fadeOut();
			$("#login-box").fadeOut();
			$("#reset-password-box").fadeOut();
		},
		showCreateAccountModal() {
			$("#login-box").fadeOut();
			this.sendAction("showCreateAccountModal");
		},
		showResetPasswordModal() {
			$("#login-box").fadeOut();
			$("#reset-password-email").focus();
			$("#reset-password-box").fadeIn();
		}
	},
	didInsertElement() {
		var firstName = $("#create-account-first-name"), lastName = $("#create-account-last-name"),
			email = $("#create-account-email"), confirmEmail = $("#create-account-email-confirm"),
			password = $("#create-account-password"), confirmPassword = $("#create-account-password-confirm"),
			loginEmail = $("#login-email"), loginPassword = $("#login-password");

		loginEmail.keyup(function(event) {
			if(loginEmail.val().length > 3 && loginPassword.val().length > 1) {
				$("#login-button").attr("disabled", false);
				checkIfEnterWasPressed(event, function() {
					$("#login-button").click();
				});
			} else {
				$("#login-button").attr("disabled", "disable");
			}
		});

		loginPassword.keyup(function(event) {
			if(loginEmail.val().length > 3 && loginPassword.val().length > 1) {
				$("#login-button").attr("disabled", false);
				checkIfEnterWasPressed(event, function() {
					$("#login-button").click();
				});
			} else {
				$("#login-button").attr("disabled", "disable");
			}

		});


	}
});

function checkIfEnterWasPressed(event, callback) {
	if (event.keyCode === 13) {
		if (callback) {
			callback();
		} else {
			$("#create-account-button").click();
		}
	}
}

function changeAccountButton(status) {
	$("#create-account-button").attr("disabled", (status === "disable" ? status : false));
}

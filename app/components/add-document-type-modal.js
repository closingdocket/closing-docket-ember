import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		addDocumentType() {
			var self = this,
				dataObj = {
					name: $("#add-document-type-name").val(),
					read: $("#add-document-read-permissions").val().join(","),
					update: $("#add-document-update-permissions").val().join(","),
					autocreate: $("#add-document-types-autocreate").prop("checked"),
					active: $("#add-document-types-active").prop("checked")
				};

			console.log("adding:", dataObj);

			Ember.$.ajax({
				method: "POST",
				url: "https://app-closingdocket.rhcloud.com/doctype?session=" + document.userCache.session,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						"active": dataObj.active,
						"autocreate": dataObj.autocreate,
						"name": dataObj.name,
						"read": dataObj.read,
						"update": dataObj.update
					}
				),
				success: function(data) {
					$("#edit-document-type-dropdown").append(
						"<option value='" + dataObj.name
						+ "|" + dataObj.read + "|" + dataObj.update + "|" + dataObj.autocreate
						+ "|" + dataObj.active + "|" + data.id + "'>" + dataObj.name + "</option>");

					$("#add-document-type-name").val("");
					$("#add-document-read-permissions").dropdown("set exactly", []);
					$("#add-document-update-permissions").dropdown("set exactly", []);
					$("#add-document-types-autocreate").prop("checked", false);
					$("#add-document-types-active").prop("checked", false)
					console.log("Document type created!");
				},
				fail: function() {
					console.warn("Document creation failed.");
				}
			});
		}
	}
});

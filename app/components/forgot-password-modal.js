import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com";

export default Ember.Component.extend({
	actions: {
		resetPassword() {
			var email = $("#reset-password-email").val();

			$("#reset-password-loader").fadeIn();
			Ember.$.ajax({
				method: "GET",
				url: `${coreURL}/user/chgpwd?email=${email}`,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				success: function(data) {
					console.log("Success!");
					$("#reset-password-loader").fadeOut();
					$("#reset-password-email-sent").val(email);
					$("#reset-password-content").slideUp();
					$("#reset-email-sent-content").slideDown();
					$("#reset-password-email").val("");
					$("#reset-password-button-wrapper").slideUp();
					$("#done-resetting-password-wrapper").slideDown();
				},
				error: function() {
					$("#reset-password-loader").fadeOut();
					$("#reset-password-error-message").slideDown();
				}
			});
		},
		hideModals()  {
			this.sendAction("hideModals");
		}
	},
	didInsertElement() {
		$("#reset-password-email").keyup(function(key) {
			$("#reset-password-error-message").slideUp();
			if ($(this).val().length > 5 && _.includes($(this).val(), "@") && _.includes($(this).val(), ".")) {
				$("#reset-password-button").attr("disabled", false);
				if (key.which === 13) {
					$("#reset-password-button").click();
				}
			} else {
				$("#reset-password-button").attr("disabled", "disable");
			}
		});
	}
});

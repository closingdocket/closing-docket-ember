import Ember from 'ember';

var sessionStorage = {}, cookiesArr = document.cookie.split(" "), key, value, pairArr;

_.forEach(cookiesArr, function(pair) {
	pairArr = pair.split("=");
	key = pairArr[0];
	value = pairArr[1] || "";
	value = value.length > 1 ? value.slice(0, value.length -1) : value.length > 0 ? value : undefined;

	sessionStorage[key] = value;
});

sessionStorage.getItem = function(item) {
	return sessionStorage[item];
}

sessionStorage.setItem = function(key, value) {
	sessionStorage[key] = value;
}

sessionStorage.clear = function() {
	_.forEach(sessionStorage, function(value, key) {
		sessionStorage[key] = undefined;
	});
}

export default Ember.Component.extend({
	actions: {
		inviteUser() {
			var baseURL = "https://app-closingdocket.rhcloud.com",
				sessionToken = document.userCache.session, email = $("#invite-user-email").val(),
				role = $("#invite-user-role").dropdown("get value")[0] | 0, firstName = $("#invite-user-first-name").val(),
				lastName = $("#invite-user-last-name").val();

			Ember.$.ajax({
				method: "POST",
				url: baseURL + "/contact?session=" + sessionToken,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						email: email,
						job: role,
						firstname: firstName,
						lastname: lastName
					}
				),
				success: function(data) {
				},
				error: function(data) {
					console.warn("Inviting user failed.");
				}
			});

			Ember.$.ajax({
				method: "POST",
				url: baseURL + "/email/invite?session=" + sessionToken,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						email: email
					}
				),
				success: function(data) {
					$("#contact-list-list").append(
						'<div class="ui fluid action input">' +
							'<input type="text" value="' + firstName + ' ' + lastName + '" placeholder="Search contact list ...">' +
							'<select class="ui selection dropdown manage-party-user-dropdown">' +
								'<option value="">Select Role</option>' +
								'<option ' + (role === 1 ? 'selected=""' : '') + ' value="1">Realtor</option>' +
								'<option ' + (role === 2 ? 'selected=""' : '') + ' value="2">Title Agent</option>' +
								'<option ' + (role === 3 ? 'selected=""' : '') + ' value="3">Lender</option>' +
								'<option ' + (role === 4 ? 'selected=""' : '') + ' value="4">Attorney</option>' +
								'<option ' + (role === 5 ? 'selected=""' : '') + ' value="5">Regulator</option>' +
								'<option ' + (role === 6 ? 'selected=""' : '') + ' value="6">Service Provider</option>' +
								'<option ' + (role === 7 ? 'selected=""' : '') + ' value="7">Other</option>' +
							'</select>' +
							'<div type="submit" class="ui cancel button">Remove</div>' +
						'</div>' +
						'<br/>'
					);
					$(".ui.dropdown").dropdown();
					$("#invite-user-email").val("");
					$("#invite-user-first-name").val("");
					$("#invite-user-last-name").val("");
					$("#invite-user-role").dropdown("set exactly", "empty");
					$("#successful-invite-notification").html(firstName + " " + lastName + " was invited and added to your contact list.<br>");
					$("#successful-invite-notification").slideDown().delay(1000).slideUp();
				},
				error: function(data) {
					console.warn("Adding party member failed!");
				}
			});
		}
	}
});

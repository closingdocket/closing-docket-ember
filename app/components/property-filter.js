import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['property-filter'],
	value: '',

	init() {
		this._super(...arguments);
	},

	didInsertElement: function() {

	}
});

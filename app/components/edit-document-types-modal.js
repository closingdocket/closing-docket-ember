import Ember from 'ember';

var coreURL = "https://app-closingdocket.rhcloud.com", sessionToken = document.userCache.session;

export default Ember.Component.extend({
	actions: {
		updateDocumentType() {
			var self = this,
				dataObj = {
					name: $("#edit-document-type-name").val(),
					read: $("#edit-document-read-permissions").val() ? $("#edit-document-read-permissions").val().join(",") :  "",
					update: $("#edit-document-update-permissions").val() ? $("#edit-document-update-permissions").val().join(",") : "",
					autocreate: $("#edit-permissions-autocreate").prop("checked"),
					active: $("#edit-document-types-active").prop("checked"),
					id: $("#edit-document-type-name").data("id") | 0
				};

			Ember.$.ajax({
				method: "PUT",
				url: "https://app-closingdocket.rhcloud.com/doctype?session=" + document.userCache.session + "&id=" + dataObj.id,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(
					{
						"active": dataObj.active,
						"autocreate": dataObj.autocreate,
						"name": dataObj.name,
						"read": dataObj.read,
						"update": dataObj.update
					}
				),
				success: function(data) {
					$("#document-type-box").modal("hide");
					$("#update-permission-error").slideUp();
					$("#read-permission-error").slideUp();
				},
				error: function() {
					if (!$("#edit-document-update-permissions").val()) {
						$("#update-permission-error").slideDown();
					}

					if (!$("#edit-document-read-permissions").val()) {
						$("#read-permission-error").slideDown();
					}

					console.warn("Updating document type changes failed.");
				}
			});
		},
		showAddNewDocumentType() {
			$("#add-document-type-box").modal("setting", "allowMultiple", true);
			$("#add-document-type-box").modal("setting", "transition", "vertical flip");
			$(".ui.checkbox").checkbox();
			$("#add-document-type-box").modal("show");
		},
		deleteDocumentType() {
			var documentId = $("#edit-document-type-name").data("id"),
				self = this;

			Ember.$.ajax({
				method: "DELETE",
				url: `${coreURL}/doctype?session=${sessionToken}&id=${documentId}`,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify({}),
				success: function(data) {
					self.sendAction("refreshData");
					$("#document-type-box").modal("hide");
				},
				error: function() {
					console.warn("Deleteing this document type failed.");
				}
			});
		}
	},
	didInsertElement() {
		$(".ui.dropdown").dropdown();
	}
});

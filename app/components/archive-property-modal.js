import Ember from 'ember';
import DS from "ember-data";

export default Ember.Component.extend({
	actions: {
		archiveProperty() {
			console.log("sending action!");
			this.sendAction("archiveProperty");
		}
	}
});

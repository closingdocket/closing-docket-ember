import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function() {
		var firstName = $("#create-account-first-name"), lastName = $("#create-account-last-name"),
			email = $("#create-account-email"), confirmEmail = $("#create-account-email-confirm"),
			password = $("#create-account-password"), confirmPassword = $("#create-account-password-confirm"),
			loginEmail = $("#login-email"), loginPassword = $("#login-password");


		// fix menu when passed
		$('#landing-page-header').visibility({
			once: false,
			onBottomPassed: function() {
				$('#login-header').transition('fade in');
			},
			onBottomPassedReverse: function() {
				$('#login-header').transition('fade out');
			}
		});

		$('#features-masthead').visibility({
			once: false,
			onBottomPassed: function() {
				$('#features-header').transition('fade in');
			},
			onBottomPassedReverse: function() {
				$('#features-header').transition('fade out');
			}
		});


		$('#read-permissions').dropdown({allowAdditions: true});
		$('#update-permissions').dropdown({allowAdditions: true});

		$("#create-account-success-box").focusout(function() {
			$("#create-account-success-box").fadeOut();
		});

		// create sidebar and attach to menu open
		$('.ui.sidebar').sidebar('attach events', '.toc.item');
		$('#create-account-modal').modal();

		$("#sign-up-button").click(function() {
			$('#create-account-modal').modal("show");
		});

		// $("#settings-dropdown").dropdown();
		// $("#occupation-dropdown").dropdown();
		$(".dropdown").dropdown();

	}
});



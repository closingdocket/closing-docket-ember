import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		showFeedbackModal() {
			$("#feedback-box").modal("setting", "transition", "vertical flip")
			$("#feedback-box").modal("show");
			$("#feedback-field").focus();
		}
	}
});

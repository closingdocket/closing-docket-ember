import Ember from 'ember';

export function docTypesDropdown(params/*, hash*/) {
  var name = params[0], docTypeId = params[1], docDrops = params[2], hide = false;

  // hides doctypes that are already in use
  docDrops = docDrops.filter(function(){return true;});
  _.forEach(docDrops, function(docDrop) {
  	if (docDrop.get("isMine")) {
  		if (docDrop.get("doctype") === docTypeId | 0) {
  			hide = true;
  		}
  	}
  });

  if (!hide) {
  	return new Ember.Handlebars.SafeString(`<option value="${docTypeId}">${name}</option>`);
  } else {
  	return "";
  }
}

export default Ember.Helper.helper(docTypesDropdown);

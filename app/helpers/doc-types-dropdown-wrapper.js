import Ember from 'ember';

export function docTypesDropdownWrapper(params/*, hash*/) {
	var propId = params[0];

							console.log("returning:",  `<select class="ui search selection dropdown add-new-drop-dropdown new-drop-${propId}">`);

	return new Ember.Handlebars.SafeString(`<select class="ui search selection dropdown add-new-drop-dropdown new-drop-${propId}">`);
}

export default Ember.Helper.helper(docTypesDropdownWrapper);

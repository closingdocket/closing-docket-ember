import Ember from 'ember';

export function uploadProgressBar(params/*, hash*/) {
	var propId = params[0];

	return new Ember.Handlebars.SafeString(`<div class="upload-progess" id="upload-progress-${propId}"></div>`);
}

export default Ember.Helper.helper(uploadProgressBar);

import Ember from 'ember';

export function dragAndDropInput(params/*, hash*/) {
	var company = params[0], property = params[1], doctype = params[2], userId = params[3];

	return new Ember.Handlebars.SafeString("<input class='file-upload' id='" + company + "-" + property + "-" + doctype + "' type='file' name='file' data-company='" + company  + "' data-property='" + property + "' data-doctype='" + doctype + "' data-user='" + userId + "'>");
}

export default Ember.Helper.helper(dragAndDropInput);

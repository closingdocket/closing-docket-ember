import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('login');
  // this.route('index');
  this.route('about-us', function() {
    this.route('settlement-agent');
  });
  this.route('features');
  this.route('legal');
  this.route('invite');
  this.route('validate');
  this.route('pricing');
  this.route('chgpwd');
  this.route('faq');
  this.route('security');
  this.route('quiz');
  this.route('getting-started');
});

export default Router;

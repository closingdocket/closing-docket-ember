import Ember from 'ember';

export default Ember.Route.extend({
	beforeModel() {
		ga('send', 'pageview', 'legal');
	},
	actions: {
		showCreateAccountModal() {
			$("#login-box").fadeOut();
			$("#create-account-box").fadeIn();
			$("#create-account-first-name").focus();
			$("#create-account-success-box").fadeOut();
		},
		showLoginModal() {
			$("#create-account-box").fadeOut();
			$("#login-box").fadeIn();
			$("#create-account-success-box").fadeOut();
			$("#login-email").focus();
		},
		hideModals() {
			$("#create-account-box").fadeOut();
			$("#login-box").fadeOut();
		},
		goToFeatures() {
			this.transitionTo("features");
		},
		goToAboutUs() {
			this.transitionTo("about-us");
		},
		goToLogin() {
			this.transitionTo("login");
		},
		createAccount() {
			var self = this;
			Ember.$.ajax({
			  method: "POST",
			  url: "https://app-closingdocket.rhcloud.com/user",
			  dataType: "json",
			  contentType: "application/json; charset=utf-8",
			  data: JSON.stringify(
			  	{
			  		"firstname": $("#create-account-first-name").val(),
			  		"lastname": $("#create-account-first-name").val(),
			  		"email": $("#create-account-email").val(),
			  		"job": $("#create-account-job").val() | 0 || 7,
			  		"password": $("#create-account-password").val(),
			  		"password2": $("#create-account-password-confirm").val()
			  	}
		  	),
			  success: function(data) {
			  	$("#create-account-box").fadeOut();
			  	$("#validate-email-location").html($("#create-account-email").val());
			  	$("#create-account-success-box").fadeIn();
			  },
			  fail: function() {
			  	console.warn("account creation failed");
			  		//TODO: test this
			  		// Ember.$(document).ready(function() {
			  		// 		Ember.$("#login-fail-modal").show();
			  		// });
			  }
			});

		},
		login() {
			var self = this;
			Ember.$.ajax({
			  method: "PUT",
			  url: "https://app-closingdocket.rhcloud.com/user/signin/email",
			  dataType: "json",
			  contentType: "application/json; charset=utf-8",
			  data: JSON.stringify({"email": $("#login-email").val(), "password": $("#login-password").val()}),
			  success: function(data) {
		  		document.userCache = data;
		  		self.transitionTo("index");
			  },
			  fail: function() {
			  		//TODO: test this

			  }
			});
		}
	}
});

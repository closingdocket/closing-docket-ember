import Ember from "ember";

export default Ember.Route.extend({
	beforeModel() {
		var args = window.location.search.split("&"),
			argsObj = {}, key, value;

		_.forEach(args, function(arg) {
			key = arg.split("=")[0].replace(/\//gi, "").replace(/\?/gi, "");
			value = arg.split("=")[1];
			argsObj[key] = value;
		});

		if (argsObj.first && argsObj.last && argsObj.email) {
			document.inlineAccountCreation = argsObj;
		}

		if (!document.inlineAccountCreation) {
			this.transitionTo("login");
		}
	},
	model() {
		const {
			first,
			last,
			job
		} = document.inlineAccountCreation;

		return {
			first: _.upperFirst(first),
			last: _.upperFirst(last),
			job: job
		};
	}
});

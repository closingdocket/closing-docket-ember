import Ember from 'ember';
// import DS from 'embae'

export default Ember.Route.extend({
  beforeModel() {
    (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
    })(window,document,"script","https://www.google-analytics.com/analytics.js","ga");

    ga("create", "UA-75212249-1", "auto");
    ga("send", "pageview", "quiz");
  },
  actions: {
    advanceToGettingStarted() {
      ga("send", "event", "quiz", "click to go to getting-started");
      document.inlineAccountCreation.first = $("#first").val();
      document.inlineAccountCreation.last = $("#last").val();
      this.transitionTo("getting-started");
    }
  },
  model() {
    return Ember.A([
      {
        question: 'What role would you consider yourself?',
        answers: [
          'Attorney',
          'Title agent',
          'Other'
        ]
      },
      {
        question: 'How often have your closings gone according to plan?',
        answers: [
          'Less than half the time',
          'Around half the time',
          'Almost all the time'
        ]
      },
      {
        question: 'How rarely are you contacted by another member of the party regarding the status of the closing?',
        answers: [
          'Almost nver',
          'A couple times a week',
          'Multiple times a day'
        ]
      },
      {
        question: 'How often do you have the correct version of all the documents at the closing table?',
        answers: [
          'Very often',
          'Most of the time',
          'Sometimes'
        ]
      },
      {
        question: 'How do you share your documents?',
        answers: [
          'Email/encrypted email',
          'File-sharing software',
          'Other'
        ]
      },
      {
        question: 'How many closings are you currently involved in each month?',
        answers: [
          'Less than 5',
          '5-10',
          'More than 10'
        ]
      }
    ]);
  }
});

import Ember from "ember";

export default Ember.Route.extend({
	beforeModel() {
		var ident = _.last(window.location.search.split("=")),
			coreURL = "https://app-closingdocket.rhcloud.com", self = this;

		(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
		})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");

		ga("create", "UA-75212249-1", "auto");
		ga("send", "pageview", "validate");

		Ember.$.ajax({
			method: "PUT",
			url: `${coreURL}/user/validate/?ident=${ident}`,
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify({}),
			success: function(data) {
				document.userCache = data;
				if (sessionStorage) {
					sessionStorage.setItem("session", data.session);
					sessionStorage.setItem("company", data.company);
					sessionStorage.setItem("user", data.user);
					sessionStorage.setItem("admin", data.admin);
				}
				self.transitionTo("index");
			},
			error: function() {
				console.warn("Validation did not work. Contact support@closingdocket.com for help with your account.");
				self.transitionTo("login");
			}
		});
	}
});

import Ember from "ember";
import DS from "ember-data";
import _ from "lodash/lodash";

var coreURL = "https://app-closingdocket.rhcloud.com", sessionToken;

export default Ember.Route.extend({
    beforeModel() {
        var self = this;

      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-75212249-1', 'auto');
      ga('send', 'pageview', 'index');

        if (sessionStorage) {
            document.userCache = {
                session: sessionStorage.getItem("session"),
                company: sessionStorage.getItem("company"),
                user: sessionStorage.getItem("user"),
                admin: sessionStorage.getItem("admin")
            };
        }

        if (!document.userCache) {
            this.transitionTo("login");
        } else {
            sessionToken = document.userCache.session;
            Ember.$.ajax({
                method: "GET",
                url: coreURL + "/user?session=" + sessionToken,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function() {
                },
                error: function() {
                    // console.warn("Session token given cached was wrong. Redirecting to login.");
                    self.transitionTo("login");
                    document.userCache = {};
                }
            });
        }
    },
    archiveMode: false,
    compactMode: false,
    userHasProperties: false,
    actions: {
        zoomIntoProperty(searchString) {
            ga('send', 'event', "compact view", "toggled", "off");
            this.set("compactMode", false);
            this.get("store").unloadAll();
            this.transitionTo("login");
            this.transitionTo("index");

            setTimeout(function() {
                $("#property-filter").val(searchString);
                $("#property-filter").trigger("keyup");
            }, 700);
        },
        addProperty(name) {
            ga('send', 'event', "property", "added", name);

            var self = this;

            if (name.length) {
                Ember.$.ajax({
                    method: "POST",
                    url: coreURL + "/property?session=" + sessionToken,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({"street": name }),
                    success: function(data) {
                        self.get("store").unloadAll();
                        self.transitionTo("login");
                        self.transitionTo("index");
                        setTimeout(function() {
                            $("#property-filter").val(name);
                            $("#property-filter").trigger("keyup");
                        }, 1000);
                    },
                    error: function(data) {
                        // console.log("fail:", data);
                        // console.warn("Adding property failed.");
                    }
                });
            }
        },
        toggleViewMode() {
            if (this.get("compactMode")) {
                ga('send', 'event', "compact view", "toggled", "off");
                this.set("compactMode", false);
                this.get("store").unloadAll();
                this.transitionTo("login");
                this.transitionTo("index");
            } else {
                ga('send', 'event', "compact view", "toggled", "on");
                this.set("filterValue", undefined);
                this.set("compactMode", true);
                this.get("store").unloadAll();
                this.transitionTo("login");
                this.transitionTo("index");
                $("#property-filter").val("");
                $("#property-filter").trigger("keyup");
            }
        },
        toggleArchive() {
            if (this.get("archiveMode")) {
                ga('send', 'event', "archive view", "toggled", "off");
                this.set("archiveMode", false);
                window.location.reload();
            } else {
                ga('send', 'event', "archive view", "toggled", "on");
                this.set("archiveMode", true);
                window.location.reload();
            }
        },
        refreshData() {
            this.get("store").unloadAll();
            this.transitionTo("login");
            this.transitionTo("index");
            setTimeout(function() {
                $(".ui.dropdown").dropdown();
            }, 1000);
        },
        addDrop(property, docType) {
            ga('send', 'event', "doc drop", "added", docType);

            var self = this,
                propId = property.get("propId"),
                company = property.get("company");

            Ember.$.ajax({
                method: "POST",
                url: coreURL + "/docdrop?session=" + sessionToken,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(
                    {
                        company: company | 0,
                        doctype: docType | 0,
                        property: propId | 0
                    }
                ),
                success: function(data) {
                    window.location.reload();
                },
                error: function() {
                    // console.warn("Adding a DocDrop failed.");
                }
            });
        },
        addTask() {
            var companyId = $("#new-task-description").data("company"),
                propertyId = $("#new-task-description").data("property"),
                description = $("#new-task-description").val(),
                self = this;

            if ($("#new-task-description").val().length > 0) {
                ga('send', 'event', "task", "added", $("#new-task-description").val());

                Ember.$.ajax({
                    method: "POST",
                    url: coreURL + "/task?session=" + sessionToken,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(
                        {
                            company: companyId,
                            property: propertyId,
                            todo: description
                        }
                    ),
                    success: function() {
                        $(".user-owned#task-list-items").append(
                            '<div class="item">' +
                                '<div class="ui checkbox">' +
                                    '<input type="checkbox" tabindex="0" class="hidden">' +
                                    '<label class="list-item">' + description + '</label>' +
                                '</div>' +
                            '</div>'
                        );

                        $("#new-task-description").val("");
                    },
                    error: function() {
                        // console.warn("Adding a task failed.");
                    }
                });
            } else {
                // console.warn("No task description given.");
            }
        },
        deleteDocument(property, docDrop) {
            var companyId = property.get("company"),
                propertyId = property.get("propId"),
                userId = docDrop.get("user"),
                docTypeId = docDrop.get("doctype"),
                self = this;

            ga('send', 'event', "document", "deleted", docTypeId);

            Ember.$.ajax({
                method: "DELETE",
                url: coreURL + "/docdrop/doc?session=" + sessionToken + "&company=" + companyId + "&property=" + propertyId + "&user=" + userId + "&doctype=" + docTypeId,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({}),
                success: function(data) {
                    window.location.reload();
                },
                error: function() {
                    // console.warn("Deleting a document failed.");
                }
            });
        },
        deleteDrop(property, docDrop) {
            var companyId = property.get("company"),
                propertyId = property.get("propId"),
                docTypeId = docDrop.get("doctype"),
                self = this;

            ga('send', 'event', "document", "delete", docTypeId);

            Ember.$.ajax({
                method: "DELETE",
                url: coreURL + "/docdrop?session=" + sessionToken + "&company=" + companyId + "&property=" + propertyId + "&doctype=" + docTypeId,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({}),
                success: function(data) {
                    window.location.reload();
                },
                error: function() {
                    // console.warn("Deleting a drop failed");
                }
            });
        },
        archiveProperty() {
            var companyId = $("#archive-property-button").data("company"),
                propertyId = $("#archive-property-button").data("property"),
                self = this;

            ga('send', 'event', "property", "archive", "property id: " + propertyId);

            Ember.$.ajax({
                method: "PUT",
                url: coreURL + "/property/archive?session=" + sessionToken + "&company=" + companyId + "&property=" + propertyId,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({}),
                success: function(data) {
                    window.location.reload();
                },
                error: function() {
                    // console.warn("Archiving a property failed.");
                }
            });
        },
        signOut() {
            ga('send', 'event', "session", "end", "user: " + document.userCache.user);

            document.userCache = {};

            if (sessionStorage) {
                sessionStorage.clear();
            }

            this.transitionTo("login");
        },
        showEditCompanyProfileModal() {
            ga('send', 'event', "company profile", "view");

            $("#edit-company-profile-box").modal("setting", "transition", "vertical flip")
            $("#edit-company-profile-box").modal("show");
        },
        showUpgradeAccountModal() {
            ga('send', 'event', "upgrade account", "view");

            $("#upgrade-account-box").modal("setting", "transition", "vertical flip")
            $("#upgrade-account-box").modal("show");
        },
        showFAQModal() {
            ga('send', 'event', "FAQ", "view");

            $("#faq-box").modal("setting", "transition", "vertical flip")
            $("#faq-box").modal("show");
        },
        showEditProfileModal(userModel) {
            ga('send', 'event', "user profile", "view");

            var jobId = userModel.get("job");

            $("#occupation-dropdown").dropdown("set exactly", [jobId]);

            $("#edit-profile-box").modal("setting", "transition", "vertical flip")
            $("#edit-profile-box").modal("show");
        },
        showSiteStats() {
            ga('send', 'event', "site stats", "view");

            $("#site-stats-box").modal("setting", "transition", "vertical flip")
            $("#site-stats-box").modal("show");
        },
        showNewStuff() {
            ga('send', 'event', "new stuff", "view");

            $("#new-stuff-box").modal("setting", "transition", "vertical flip")
            $("#new-stuff-box").modal("show");
        },
        viewContacts(contacts) {
            ga('send', 'event', "contacts", "view");

            // start loader
            $("#index-loader").fadeIn();

            var counter = 1, cdMemberContactList = [], cdInvitedContactList = [], contactTemplate = _.template(
                '<div class="ui fluid action input">' +
                    '<input type="text" value="<%= name %>" disabled style="color: black !important;" data-userid="<%= userId %>" class="ui fluid contact-list-name">' +
                    '<div type="submit" id="contact-remove-<%= userId %>" data-userid="<%= userId %>" class="ui cancel button">Remove</div>' +
                '</div>' +
                '<br/>'
                ),
                sessionToken = document.userCache.session, coreURL = "https://app-closingdocket.rhcloud.com",
                company = document.userCache.company | 0, self = this;

            $("#contact-list-list").html("");

            Ember.$.getJSON(coreURL + "/contact/list?session=" + sessionToken).then(function (contacts) {
                _.forEach(contacts, function(contact) {
                    if (contact.user) {
                        cdMemberContactList.push({
                            company: contact.company,
                            contactId: contact.id,
                            user: contact.user,
                            name: contact.firstname + " " + contact.lastname,
                            email: contact.email,
                            job: contact.job,
                            phone: contact.phone,
                            fax: contact.fax,
                            nmls: contact.nmls,
                            license: contact.license
                        });
                    } else {
                        cdInvitedContactList.push({
                            company: contact.company,
                            contactId: contact.id,
                            user: contact.user,
                            name: contact.firstname + " " + contact.lastname,
                            email: contact.email,
                            job: contact.job,
                            phone: contact.phone,
                            fax: contact.fax,
                            nmls: contact.nmls,
                            license: contact.license
                        });
                    }
                });

                _.forEach(cdInvitedContactList, function(contact, idx) {
                    counter = idx + 1;

                    $("#invited-contact-list").append(
                        contactTemplate({
                            name: contact.name,
                            userId: contact.contactId,
                            realtor: contact.role === 1 ? "selected=''" : "",
                            title: contact.role === 2 ? "selected=''" : "",
                            lender: contact.role === 3 ? "selected=''" : "",
                            attorney: contact.role === 4 ? "selected=''" : "",
                            regulator: contact.role === 5 ? "selected=''" : "",
                            service: contact.role === 6 ? "selected=''" : "",
                            other: contact.role === 7 ? "selected=''" : "",
                            trustLevel0: contact.trust === 0 ? "selected=''" : "",
                            trustLevel1: contact.trust === 1 ? "selected=''" : "",
                            trustLevel2: contact.trust === 2 ? "selected=''" : ""
                        })
                    );
                });

                _.forEach(cdMemberContactList, function(contact, idx) {
                    counter += 1;

                    $("#member-contact-list").append(
                        contactTemplate({
                            name: contact.name,
                            userId: contact.contactId,
                            realtor: contact.role === 1 ? "selected=''" : "",
                            title: contact.role === 2 ? "selected=''" : "",
                            lender: contact.role === 3 ? "selected=''" : "",
                            attorney: contact.role === 4 ? "selected=''" : "",
                            regulator: contact.role === 5 ? "selected=''" : "",
                            service: contact.role === 6 ? "selected=''" : "",
                            other: contact.role === 7 ? "selected=''" : "",
                            trustLevel0: contact.trust === 0 ? "selected=''" : "",
                            trustLevel1: contact.trust === 1 ? "selected=''" : "",
                            trustLevel2: contact.trust === 2 ? "selected=''" : ""
                        })
                    );
                });

                _.forEach(cdInvitedContactList, function(contact) {
                    $("#contact-remove-" + contact.contactId).click(function() {
                        var contact = $(this).data("userid");
                        $($("#contact-remove-" + contact).parent()).slideUp();
                        Ember.$.ajax({
                            method: "DELETE",
                            url: `${coreURL}/contact?session=${sessionToken}&contact=${contact}`,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function(data) {
                            },
                            fail: function() {
                                // console.warn("Removing contact failed.");
                            }
                        });
                    });
                });

                _.forEach(cdMemberContactList, function(contact) {
                    $("#contact-remove-" + contact.contactId).click(function() {
                        var contact = $(this).data("userid");
                        $($("#contact-remove-" + contact).parent()).slideUp();
                        Ember.$.ajax({
                            method: "DELETE",
                            url: `${coreURL}/contact?session=${sessionToken}&contact=${contact}`,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function(data) {
                            },
                            fail: function() {
                                // console.warn("Removing contact failed.");
                            }
                        });
                    });
                });

                if (!counter) {
                    $("#contact-list-list").prepend(
                        "<div style='width: 100%; text-align: center; margin-bottom: 20px;'>" +
                            "<i class='fa fa-exclamation fa-3x' aria-hidden='true'></i><br>" +
                            "<span style='font-size: 25px;'>Looks like you don't have anybody assigned to this closing!</span><br/><br/>" +
                            "Assign yourself to the role that best fits your job description in this closing.<br>" +
                            "Then, either add someone from your contact list or type in an email address to invite someone to this closing." +
                        "</div>"
                    );
                }

                // end loader
                $("#index-loader").fadeOut();
                $(".ui.dropdown").dropdown();
                $("#contact-list-box").modal("setting", "transition", "vertical flip");
                $("#contact-list-box").modal("show");
                $(".title-popup").popup();
            });

        },
        showEditDocumentTypesModal(doctypes) {
            ga('send', 'event', "document types", "view");

            var docs = doctypes.filter(function() {
                return true;
            });

            _.forEach(docs, function(doc) {
                var name = doc.get("name"), read = doc.get("read"), update = doc.get("update"),
                    autocreate = doc.get("autocreate"), active = doc.get("active"),
                    id = doc.get("docTypeId");

                $("#edit-document-type-dropdown").append(
                    "<option value='" + name
                    + "|" + read + "|" + update + "|" + autocreate
                    + "|" + active + "|" + id + "'>" + name + "</option>")
            });
            $("#edit-document-type-dropdown").change(function() {
                var data = $("#edit-document-type-dropdown").val().split("|"),
                    dataObj = {
                        name: data[0],
                        read: data[1].split(","),
                        update: data[2].split(","),
                        autocreate: !!(data[3] | 0),
                        active: !!(data[4] | 0),
                        id: data[5] | 0
                    };


                $("#edit-document-type-name").val(dataObj.name);
                $("#edit-document-type-name").data("id", dataObj.id);
                _.forEach(["read", "update"], function(permissionType) {
                    $("#edit-document-" + permissionType + "-permissions").dropdown("set exactly", dataObj[permissionType]);
                });

                $("#edit-document-types-active").prop("checked", dataObj.active ? "checked" : false);
                $("#edit-document-types-autocreate").prop("checked", dataObj.autocreate ? "checked" : false);
            });

            $("#document-type-box").modal("setting", "allowMultiple", true);
            $(".ui.checkbox").checkbox();
            $("#document-type-box").modal("setting", "transition", "vertical flip");
            $("#document-type-box").modal("show");
        }
    },
    model() {
        var documents = {}, self = this;

        if (document.userCache.session) {
            // get contact list
            Ember.$.getJSON(coreURL + "/contact/list?session=" + sessionToken).then(function (contactList) {
                _.forEach(contactList, function(contact) {
                    self.get("store").createRecord("contact", {
                        company: contact.company,
                        contactId: contact.id,
                        user: contact.user,
                        firstname: contact.firstname,
                        lastname: contact.lastname,
                        email: contact.email,
                        job: contact.job,
                        phone: contact.phone,
                        fax: contact.fax,
                        nmls: contact.nmls,
                        license: contact.license
                    });
                });
            });

            // get doctypes
            return Ember.$.getJSON(coreURL + "/doctype/list?session=" + sessionToken).then(function (docs) {
                _.forEach(docs, function(doc) {
                    documents[doc.id] = doc.name;
                    self.get("store").createRecord("doctype", {
                        user: doc.user,
                        docTypeId: doc.id,
                        name: doc.name,
                        read: doc.read,
                        update: doc.update,
                        active: doc.active,
                        autocreate: doc.autocreate
                    });
                });
            })
            .then(function() {
                return Ember.$.getJSON(coreURL + "/property/list?session=" + sessionToken + (self.get("archiveMode") ? "&archive=true" : ""));
            })
            .then(function (properties) {
                if (properties.length > 0) {
                    self.set("userHasProperties", true);
                }
                return properties.map(function(property) {
                    return Property.create(property);
                });
            })
            .then(function(properties) {
                _.forEach(properties, function(property) {
                    property.set("docDrops", Ember.A());
                    property.set("party", Ember.A());
                    property.set("tasks", Ember.A());

                    var percentage, counter,
                        totalCounter = 0,
                        currentCounter = 0,
                        userInParty = false,
                        userRole = undefined;

                    // get parties
                    Ember.$.getJSON(coreURL + "/party/list?session=" + sessionToken + "&company=" + property.company + "&property=" + property.id + (self.get("archiveMode") ? "&archive=true" : "")).then(function (party) {
                        _.forEach(party, function(partyMember) {
                            if (partyMember.user === (document.userCache.user | 0)) {
                                userInParty = true;
                                userRole = partyMember.role;
                            }

                            property.get("party").pushObject(Party.create(partyMember));
                        });
                    }).then(function() {
                        // get task list
                        Ember.$.getJSON(coreURL + "/task/list?session=" + sessionToken + "&company=" + property.company + "&property=" + property.id + (self.get("archiveMode") ? "&archive=true" : "")).then(function (taskList) {
                            _.forEach(taskList, function(task) {
                                property.get("tasks").pushObject(Task.create(task));
                            });
                        });

                    }).then(function() {
                        // get docdrops
                        Ember.$.getJSON(coreURL + "/docdrop/list?session=" + sessionToken + "&company=" + property.company + "&property=" + property.id + (self.get("archiveMode") ? "&archive=true" : "")).then(function (docDrops) {
                            _.forEach(docDrops, function(drop) {
                                var read = drop.read.split(","),
                                    update = drop.update.split(",");
                                drop.name = documents[drop.doctype];
                                drop.docExists = !!drop.docname;

                                if (!!drop.docname) {
                                    currentCounter += 1;
                                }
                                totalCounter += 1;

                                drop.canUpdate = userRole ? _.includes(update, userRole.toString()) : undefined;
                                drop.canRead = userRole ? _.includes(read, userRole.toString()) : undefined;
                                drop.isMine = (document.userCache.user | 0) === drop.user ? 1 : 0;
                                drop.color = drop.docExists ? "green" : "red";
                                drop.docHeight = drop.docExists ? "" : "flex-direction: false !important; height: 45px !important;";
                                drop.url = drop.docname ? ("https://app-closingdocket.rhcloud.com/docdrop/doc/" + drop.docname + "." + drop.docext + "?session=" + sessionToken + "&company=" + (property.company | 0) + "&property=" + (property.id | 0) + "&user=" + drop.user + "&doctype=" + drop.doctype) : "#";
                                property.get("docDrops").pushObject(DocDrop.create(drop));
                            });

                            return property;
                        }).then(function(property){
                            // sort docDrops
                            property.docDrops = _.sortBy(property.docDrops, function(o) { return o.get("name"); });

                            // percentage of completion
                            percentage = totalCounter ? ((currentCounter / totalCounter) * 100).toFixed(0).toString() : "0";
                            // num of num
                            counter = (totalCounter ? (currentCounter + " of " + totalCounter) : "0 of 0");

                            // now, create the property object
                            self.get("store").createRecord("property", {
                                propId: property.id,
                                price: property.price,
                                role: property.role,
                                state: property.state,
                                canBeModified: property.company === (document.userCache.company | 0),
                                street: property.street,
                                street2: property.street2,
                                zipcode: property.zipcode,
                                company: property.company,
                                closedate: property.closedate,
                                userInParty: userInParty,
                                docDrops: property.docDrops,
                                tasks: property.tasks,
                                party: property.party,
                                city: property.city,
                                color: (percentage | 0) < 25 ? "grey" : ((percentage | 0) === 100) ? "#21BA45" : "#E6BB48",
                                percentage: percentage,
                                count: counter
                            });
                        });
                    });
                });

                return Ember.RSVP.hash({
                    user: Ember.$.getJSON(coreURL + "/user?session=" + sessionToken).then(function (userInfo) {
                        return Ember.Object.create({
                            email: userInfo.email,
                            admin: document.userCache.admin | 0,
                            firstname: userInfo.firstname,
                            lastname: userInfo.lastname,
                            userId: userInfo.id,
                            job: userInfo.job,
                            license: userInfo.license,
                            nmls: userInfo.nmls,
                            phone: userInfo.phone
                        });
                    }),
                    company: Ember.$.getJSON(coreURL + "/company?session=" + sessionToken).then(function (companyInfo) {
                        return Ember.Object.create({
                            city: companyInfo.city,
                            fax: companyInfo.fax,
                            companyId: companyInfo.id,
                            license: companyInfo.license,
                            name: companyInfo.name,
                            nmls: companyInfo.nmls,
                            state: companyInfo.state,
                            street: companyInfo.street,
                            street2: companyInfo.street2,
                            zipcode: companyInfo.zipcode
                        });
                    }),
                    archiveMode: self.get("archiveMode"),
                    compactMode: self.get("compactMode"),
                    userHasProperties: self.get("userHasProperties"),
                    doctypes: self.get("store").findAll("doctype"),
                    properties: self.get("store").findAll("property"),
                    contacts: self.get("store").findAll("contact"),
                    statistics: (!!(document.userCache.admin | 0) ? Ember.$.getJSON(coreURL + "/user/stats?session=" + sessionToken).then(function (statistics) {
                        return Ember.Object.create({
                            users: statistics.users,
                            active: statistics.active,
                            trans: statistics.trans,
                            reg: statistics.reg,
                            docs: statistics.docs
                        });
                    }) : {})
                });
            });
        }
    },
    setupController(controller, model) {
        if (model) {
            controller.set("archiveMode", model.archiveMode);
            controller.set("compactMode", model.compactMode);
            controller.set("user", model.user);
            controller.set("userHasProperties", model.userHasProperties);
            controller.set("company", model.company);
            controller.set("doctype", model.doctypes);
            controller.set("properties", model.properties);
            controller.set("contacts", model.contacts);
            controller.set("statistics", model.statistics);
        }
        this._super(...arguments);
    }
});

var DocType = Ember.Object.extend({
    user: undefined,
    docTypeId: undefined,
    name: undefined,
    read: undefined,
    update: undefined,
    active: undefined,
    autocreate: undefined
});

var Task = Ember.Object.extend({
    id: undefined,
    todo: undefined,
    user: undefined,
    complete: undefined,
    lastname: undefined,
    firstname: undefined
});

var Party = Ember.Object.extend({
    contactCo: undefined,
    contact: undefined,
    role: undefined,
    user: undefined,
    firstname: undefined,
    lastname: undefined,
    email: undefined,
    job: undefined,
    phone: undefined,
    fax: undefined,
    nmls: undefined,
    license: undefined,
    trust: undefined
});

var Contact = Ember.Object.extend({
    company: undefined,
    contactId: undefined,
    user: undefined,
    firstname: undefined,
    lastname: undefined,
    email: undefined,
    canBeModified: undefined,
    job: undefined,
    phone: undefined,
    fax: undefined,
    nmls: undefined,
    license: undefined
});

var Property = Ember.Object.extend({
    companyId: undefined,
    propertyId: undefined,
    searchTerm: undefined,
    street: undefined,
    closedate: undefined,
    userInParty: undefined,
    docDrops: undefined,
    tasks: undefined,
    color: undefined,
    percentage: undefined,
    counter: undefined,
    party: undefined
});

var DocDrop = Ember.Object.extend({
    documentId: undefined,
    lender: undefined,
    morgage: undefined,
    buyerAgent: undefined,
    listingAgent: undefined,
    titleAgent: undefined,
    buyer: undefined,
    seller: undefined,
    regulator: undefined,
    service: undefined,
    isMine: undefined,
    settlementAgent: undefined,
    associate: undefined
});

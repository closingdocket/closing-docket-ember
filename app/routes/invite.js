import Ember from "ember";

const coreURL = "https://app-closingdocket.rhcloud.com";

export default Ember.Route.extend({
	beforeModel() {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-75212249-1', 'auto');
		ga('send', 'pageview', 'login');
	},
	actions: {
		showCreateAccountModal() {
			ga('send', 'event', "create account modal", "view");

			$("#login-box").fadeOut();
			$("#create-account-success-box").fadeOut();
			$("#create-account-box").fadeIn();
			$("#create-account-first-name").focus();
		},
		showLoginModal() {
			$("#create-account-box").fadeOut();
			$("#create-account-success-box").fadeOut();
			$("#login-box").fadeIn();
			$("#login-email").focus();
		},
		hideModals() {
			$("#create-account-success-box").fadeOut();
			$("#create-account-box").fadeOut();
			$("#login-box").fadeOut();
			$("#reset-password-box").fadeOut();
			$("#change-password-box").fadeOut();
		},
		goToFeatures() {
			this.transitionTo("features");
		},
		goToPricing() {
			this.transitionTo("pricing");
		},
		goToAboutUs() {
			this.transitionTo("about-us");
		},
		goToFAQ() {
			this.transitionTo("faq");
		},
		goToSecurity() {
			this.transitionTo("security");
		},
		createAccount() {
			ga('send', 'event', "account", "created", "name: " + $("#create-account-first-name").val() + " " + $("#create-account-last-name").val());

			$("#login-loader").fadeIn();
			var self = this;

			Ember.$.ajax({
			  method: "POST",
			  url: "https://app-closingdocket.rhcloud.com/user",
			  dataType: "json",
			  contentType: "application/json; charset=utf-8",
			  data: JSON.stringify(
			  	{
			  		"firstname": $("#create-account-first-name").val(),
			  		"lastname": $("#create-account-first-name").val(),
			  		"email": $("#create-account-email").val(),
			  		// TO DO implement phone number on account creation
			  		// phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
			  		"job": ($("#create-account-job").val() | 0) || 7,
			  		"password": $("#create-account-password").val(),
			  		"password2": $("#create-account-password-confirm").val()
			  	}
		  	),
			  success: function(data) {
			  	$("#login-loader").fadeOut();
			  	$("#create-account-box").fadeOut();
			  	$("#validate-email-location").html($("#create-account-email").val());
			  	$("#create-account-success-box").fadeIn();
			  },
			  error: function() {
			  	// console.warn("account creation failed");
			  	$("#login-loader").fadeOut();
			  	$("#create-account-error-message").slideDown();
			  }
			});
		},
		login() {
			ga('send', 'event', "account", "login", "email: ", $("#login-email").val());

			$("#login-loader").fadeIn();
			var self = this;

			Ember.$.ajax({
			  method: "PUT",
			  url: "https://app-closingdocket.rhcloud.com/user/signin/email",
			  dataType: "json",
			  contentType: "application/json; charset=utf-8",
			  data: JSON.stringify(
			  	{
			  		"email": $("#login-email").val(),
			  		"password": $("#login-password").val()
			  	}
		  	),
			  success: function(data) {
			  	ga('send', 'event', "account", "login", "SUCCESS! email: ", $("#login-email").val());
			  	$("#login-loader").fadeOut();
			  	document.userCache = data;
			  	if (sessionStorage) {
				  	sessionStorage.setItem("session", data.session);
				  	sessionStorage.setItem("company", data.company);
				  	sessionStorage.setItem("user", data.user);
				  	sessionStorage.setItem("admin", data.admin);
			  	}
		  		self.transitionTo("index");
			  },
			  error: function() {
			  	ga('send', 'event', "account", "login", "ERROR! email: ", $("#login-email").val());
			  	$("#login-loader").fadeOut();
			  	console.warn("Login failed - credentials are wrong.");
			  	$("#login-error-message").slideDown();
			  }
			});
		}
	}
});

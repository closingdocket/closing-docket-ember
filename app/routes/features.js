import Ember from 'ember';

export default Ember.Route.extend({
	beforeModel() {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-75212249-1', 'auto');
		ga('send', 'pageview', 'features');
	},
	actions: {
		goToLogin() {
			this.transitionTo("login");
		},
		goToAboutUs() {
			this.transitionTo("about-us");
		},
		goToPricing() {
			this.transitionTo("pricing");
		},
		goToFAQ() {
			this.transitionTo("faq");
		},
		goToSecurity() {
			this.transitionTo("security");
		},
		showCreateAccountModal() {
			// $("#login-threshold").slideUp();
			$("#login-box").fadeOut();
			$("#create-account-box").fadeIn();
			$("#create-account-first-name").focus();
			$("#create-account-success-box").fadeOut();
		},
		hideModals() {
			$("#create-account-success-box").fadeOut();
			$("#create-account-box").fadeOut();
			$("#login-box").fadeOut();
			$("#reset-password-box").fadeOut();
			$("#change-password-box").fadeOut();
		},
		showLoginModal() {
			$("#create-account-box").fadeOut();
			$("#login-box").fadeIn();
			$("#create-account-success-box").fadeOut();
			$("#login-email").focus();
		},
		goToFeatures() {
			this.transitionTo("features");
		},
		createAccount() {
			var self = this;
			Ember.$.ajax({
			  method: "POST",
			  url: "https://app-closingdocket.rhcloud.com/user",
			  dataType: "json",
			  contentType: "application/json; charset=utf-8",
			  data: JSON.stringify(
			  	{
			  		"firstname": $("#create-account-first-name").val(),
			  		"lastname": $("#create-account-first-name").val(),
			  		"email": $("#create-account-email").val(),
			  		"job": $("#create-account-job").val() | 0 || 7,
			  		"password": $("#create-account-password").val(),
			  		"password2": $("#create-account-password-confirm").val()
			  	}
		  	),
			  success: function(data) {
			  	$("#create-account-box").fadeOut();
			  	$("#validate-email-location").html($("#create-account-email").val());
			  	$("#create-account-success-box").fadeIn();
			  },
			  fail: function() {
			  	console.warn("account creation failed");
			  		//TODO: test this
			  		// Ember.$(document).ready(function() {
			  		// 		Ember.$("#login-fail-modal").show();
			  		// });
			  }
			});

		},
		login() {
			var self = this;
			Ember.$.ajax({
			  method: "PUT",
			  url: "https://app-closingdocket.rhcloud.com/user/signin/email",
			  dataType: "json",
			  contentType: "application/json; charset=utf-8",
			  data: JSON.stringify({"email": $("#login-email").val(), "password": $("#login-password").val()}),
			  success: function(data) {
		  		sessionStorage.setItem("sessionToken", data.session);
		  		self.transitionTo("index");
			  },
			  fail: function() {
			  		//TODO: test this

			  }
			});
		}
	},
	didInsertElement() {
		var firstName = $("#create-account-first-name"), lastName = $("#create-account-last-name"),
			email = $("#create-account-email"), confirmEmail = $("#create-account-email-confirm"),
			password = $("#create-account-password"), confirmPassword = $("#create-account-password-confirm"),
			loginEmail = $("#login-email"), loginPassword = $("#login-password");

		$('.masthead').visibility({
			once: false,
			onBottomPassed: function() {
				$('.fixed.menu').transition('fade in');
			},
			onBottomPassedReverse: function() {
				$('.fixed.menu').transition('fade out');
			}
		});

		loginEmail.keyup(function(event) {
			if(loginEmail.val().length > 3 && loginPassword.val().length > 1) {
				$("#login-button").attr("disabled", false);
			} else {
				$("#login-button").attr("disabled", "disable");
			}
		});

		loginPassword.keyup(function(event) {
			if(loginEmail.val().length > 3 && loginPassword.val().length > 1) {
				$("#login-button").attr("disabled", false);
			} else {
				$("#login-button").attr("disabled", "disable");
			}
		});
	}
});

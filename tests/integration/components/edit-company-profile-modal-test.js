import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('edit-company-profile-modal', 'Integration | Component | edit company profile modal', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{edit-company-profile-modal}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#edit-company-profile-modal}}
      template block text
    {{/edit-company-profile-modal}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

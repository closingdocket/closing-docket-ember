import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('archive-property-modal', 'Integration | Component | archive property modal', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{archive-property-modal}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#archive-property-modal}}
      template block text
    {{/archive-property-modal}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('unassigned-to-property-drop', 'Integration | Component | unassigned to property drop', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{unassigned-to-property-drop}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#unassigned-to-property-drop}}
      template block text
    {{/unassigned-to-property-drop}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

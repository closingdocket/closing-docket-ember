import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('edit-document-permissions-modal', 'Integration | Component | edit document permissions modal', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{edit-document-permissions-modal}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#edit-document-permissions-modal}}
      template block text
    {{/edit-document-permissions-modal}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

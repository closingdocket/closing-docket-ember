import { dragAndDropInput } from 'closing-docket/helpers/drag-and-drop-input';
import { module, test } from 'qunit';

module('Unit | Helper | drag and drop input');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = dragAndDropInput([42]);
  assert.ok(result);
});

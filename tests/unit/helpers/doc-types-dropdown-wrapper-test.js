import { docTypesDropdownWrapper } from 'closing-docket/helpers/doc-types-dropdown-wrapper';
import { module, test } from 'qunit';

module('Unit | Helper | doc types dropdown wrapper');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = docTypesDropdownWrapper([42]);
  assert.ok(result);
});

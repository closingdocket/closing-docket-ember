import { uploadProgressBar } from 'closing-docket/helpers/upload-progress-bar';
import { module, test } from 'qunit';

module('Unit | Helper | upload progress bar');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = uploadProgressBar([42]);
  assert.ok(result);
});
